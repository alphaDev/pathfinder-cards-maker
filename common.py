#!/usr/bin/python
# -*- coding: utf-8 -*-
import math
import os
import re
from hashlib import sha1
from random import randrange

import requests

TO_ROMAN = {
    0: '0',
    1: 'I',
    2: 'II',
    3: 'III',
    4: 'IV',
    5: 'V',
    6: 'VI',
    7: 'VII',
    8: 'VIII',
    9: 'IX',
}


def get_page(url):
    page_id = sha1(url.encode('utf8')).hexdigest()
    file_cache_path = 'cache/%s.html' % page_id
    if os.path.exists(file_cache_path):
        # print('load from cache : %s' % file_cache_path)
        with open(file_cache_path, 'r', encoding='utf-8') as fd:
            return fd.read()
    else:
        page = requests.get(url)
        content = page.content.decode('utf8')
        content = content.replace('</br>', '<br />')
        content = content.replace('<BR>', '<br />')
        with open(file_cache_path, 'w', encoding='utf-8') as fd:
            fd.write(content)
    
    return content


def format_distance(text):
    return re.sub(r'(\d+)\s+m\s+\((\d+)\s+c\)', r'\1m(\2c)', text)


def base_replace_in_desc(text):
    text = format_distance(text)
    auto_replace = [
        ('vingt-quatre', '24'),
        ('dix-huit', '18'),
        ('trois cents', '300'),
        ('deux cent cinquante', '250'),
        ('soixante', '60'),
        ('trente', '30'),
        ('vingt', '20'),
        ('dix', '10'),
        ('deux', '2'),
        ('six', '6'),
        ('quatre', '4'),
        ('huit', '8'),
        ('trois', '3'),
        ('centimètres', 'cm'),
        ('mètres', 'm'),
        ('mètre', 'm'),
        ('kilogrammes', 'kg'),
        ('maximale', 'max'),
        ('maximum', 'max'),
        ('minimum', 'min'),
        ('vitesse de déplacement', 'VD'),
        ('niveau de lanceur de sorts', 'NLS'),
        ('niveau de lanceur de sort', 'NLS'),
        ('niveaux de lanceur de sorts', 'NLS'),
        ('niveaux de lanceur de sort', 'NLS'),
        ('niveaux de lanceur', 'NLS'),
        ('point de vie', 'PV'),
        ('points de vie', 'PV'),
        ('points de dégâts', 'dgm'),
        ('points de dégât', 'dgm'),
        ('dégâts', 'dgm'),
        ('dégât', 'dgm'),
        ('Niveau', 'lvl'),
        ('niveau', 'lvl'),
        ('minutes', 'min'),
        ('minute', 'min'),
        ('heures', 'h'),
        ('heure', 'h'),
        ('jours', 'j'),
        ('jour', 'j'),
        ('rounds', 'rd'),
        ('round', 'rd'),
        ('jets d\'attaque et de dégâts', 'JdA & dgm'),
        ('JS', 'JdS'),
        ('points de vie', 'PV'),
        ('dés de vie', 'DV'),
        ('Constitution', 'Con'),
        ('Dextérité', 'Dex'),
        ('Charisme', 'Cha'),
        ('Force', 'For'),
        ('Intelligence', 'Int'),
        ('Sagesse', 'Sag'),
        ('jet de sauvegarde', 'JdS'),
        ('jets de sauvegarde', 'JdS'),
        ('jets d\'attaque', 'JdA'),
        ('jet d\'attaque', 'JdA'),
        ('pouvoir mythique', 'PM'),
        ('lanceur de sorts', 'personnage'),
        ('bonus de base à l\'attaque', 'BBA'),
        ('action simple', 'AS'),
        ('corps à corps', 'CàC'),
        ('dd', 'DD'),
        ('créatures', 'créa'),
        ('créature', 'créa'),
        ('personnages', 'perso'),
        ('personnage', 'perso'),
        # ('attaques', 'atk'),
        # ('attaque', 'atk'),
        # ('attaquer', 'atk'),
        ('contre', 'vs'),
        ('composante', 'compo'),
        # ('objets', 'obj'),
        # ('objet', 'obj'),
        # ('temps', 'tps'),
        # ('automatiquement', 'auto'),
        # ('automatique', 'auto'),
        # ('capacité', 'capa'),
        # ('armure', 'arm'),
        ('moitié', '½'),
        ('double', 'x2'),
        ('doublés', 'x2'),
        # ('compétences', 'comp'),
        # ('compétence', 'comp'),
        # ('toujours', 'tj'),
        ('Dans le cas contraire', 'Sinon'),
        # ('intermédiaire', 'inter'),
        # ('vitesse', 'vit'),
        ('ou encore', 'ou'),
        # ('plus', '+'),
        # ('normalement', ''),
        ('égal', '='),
        ('égale', '='),
        # ('également', ''),
        ('il lui est impossible de', 'il ne peut pas'),
        # ('altitude', 'alt'),
        # ('manoeuvrabilité', 'manœuvr'),
        # ('manœuvrabilité', 'manœuvr'),
        # ('nombre', 'nb'),
        ('jets de Réflexes', 'jets de Réf'),
        ('jet de Réflexes', 'jet de Réf'),
        ('Lorsque la durée du sort est écoulée', 'A la fin du sort'),
        ('cette incantation', 'ce sort'),
        ('''de l'incantation''', 'du sort'),
        # ('instantanément', 'insta'),
        # ('humanoïdes', 'human'),
        # ('catégorie', 'cat'),
        # ('inférieure', 'inf'),
        ('bénéficient', 'gagnent'),
        # ('propriétaire', 'proprio'),
        # ('propriétés', 'prop'),
        # ('projectiles', 'proj'),
        # ('moins', '-'),
        # ('protection', 'protect°'),
        # ('communiquer', 'com'),
        # ('capacités', 'capa'),
        ('naturelles', 'nat'),
        ('naturel', 'nat'),
        # ('supplémentaires', 'sup'),
        # ('défense', 'def'),
        ('second', '2nd'),
        # ('temporaire', 'tmp'),
        # ('direction', 'dir'),
        # ('adjacente', 'adj'),
        # ('action de mouvement', 'act° de mouv'),
        # ('action', 'act°'),
        # ('moyenne', 'moy'),
        # ('contrôlent', 'ctrl'),
        # ('contrôle', 'ctrl'),
        ('En cas de', 'si'),
        # ('dans', 'ds'),
        ('troisième', '3ème'),
        # ('points', 'pts'),
        ('par vs', 'par contre'),
        ('tour de jeu', 'tour'),
        # ('équivalent', 'équi'),
        # ('inférieures', 'inf'),
        ('''l'emporte avec lui''', '''l'emporte'''),
        ('''lanceur de sort''', '''perso'''),
        ('''x2 illusoire''', '''double illusoire'''),
    ]
    
    for k, v in auto_replace:
        text = re.sub(r'\b%s\b' % k, v, text, flags=re.IGNORECASE)
    
    return text


def build_table(table_data, game_icon_size=18, full_border=False):
    uuid = randrange(10000000)
    template = '''
    <style>
        .skill-%(uuid)s {
            font-size:10px;
            width:100%%;
            border-spacing:0;
            border-collapse: collapse;
            line-height:1.42857143;
            border-spacing:0;
            padding:0;
        }
        .skill-%(uuid)s td {
            padding:0;
        }
        .skill-%(uuid)s tr {
            border-bottom: 1px solid #3333335c;
        }
        .skill-%(uuid)s td:nth-child(2) {
            border-right: 1px solid #3333335c;
            padding-right: 2px;
        }
        .skill-%(uuid)s td:nth-child(3) {
            padding-left: 2px;
        }
        .skill-%(uuid)s .game-icon {
            line-height: %(game_icon_size)spx;
        }
        .skill-%(uuid)s tr:nth-last-child(1) {
            border-bottom: none;
        }
    </style>
    <table class='skill-%(uuid)s'><tbody>
    %(content)s
    </tbody></table>
    '''
    
    full_border_style = 'style="border-right: 1px solid #3333335c;"' if full_border else ''
    
    table_html = []
    for line in table_data:
        table_html.append('<tr>')
        for row in line:
            table_html.append('<td %s >' % full_border_style)
            table_html.append(row)
            table_html.append('</td>')
        table_html.append('</tr>')
    
    k = ' '.join(table_html)
    full_table = template % {
        'uuid'          : uuid,
        'game_icon_size': game_icon_size,
        'content'       : k,
    }
    
    return ' '.join([i.strip() for i in full_table.splitlines()])


def build_dndstats(stats, for_invo):
    template = '''
    <table class="card-stats">
        <tbody><tr>
          <th class="card-stats-header">For</th>
          <th class="card-stats-header">Dex</th>
          <th class="card-stats-header">Con</th>
          <th class="card-stats-header">Int</th>
          <th class="card-stats-header">Sag</th>
          <th class="card-stats-header">Cha</th>
        </tr>
        <tr>
          <td class="card-stats-cell">%s</td>
          <td class="card-stats-cell">%s</td>
          <td class="card-stats-cell">%s</td>
          <td class="card-stats-cell">%s</td>
          <td class="card-stats-cell">%s</td>
          <td class="card-stats-cell">%s</td>
        </tr>
      </tbody>
    </table>
    '''
    
    mods = []
    for i, stat in enumerate(stats):
        if not stat or stat == '-':
            mods.append('-')
        else:
            stat = int(stat)
            mod = math.floor(((stat - 10) / 2))
            if mod >= 0:
                mod = '+%s' % mod
            
            text = '%s(%s)' % (stat, mod)
            
            if for_invo and (i == 0 or i == 2):
                stat += 4
                mod = math.floor(((stat - 10) / 2))
                if mod >= 0:
                    mod = '+%s' % mod
                text += ' [%s(%s)]' % (stat, mod)
            mods.append(text)
    
    dnd_stats = template % tuple(mods)
    return ' '.join([i.strip() for i in dnd_stats.splitlines()])
