#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import math

from common import build_dndstats, build_table, base_replace_in_desc
from make_spell_card import SCHOOL_ICON, build_card
from make_summon_card import SENSES, format_skill, SKILL, ENERGIES_TYPE
from spell_scraper import parse_spell_page


class Perso():
    ca = '15'
    ca_touch = '13'
    ca_surprised = '13'
    saving_throw_ref = '+4'
    saving_throw_vig = '+4'
    saving_throw_vol = '+6'
    VD = '6m(4c)'
    JdA_dist = '+8'
    for_val = '9'
    dex_val = '14'
    con_val = '14'
    int_val = '14'
    sag_val = '12'
    cha_val = '20'
    size = 'P'
    raw_align = 'CB+'
    init = '+4'
    bba = '+5'
    bmo = '+3'
    dmd = '+5'
    nls = '7'
    spell_range_short = '13.5m(9c)'
    spell_range_med = '51m(34c)'
    spell_range_long = '204m(136c)'
    
    skill = {
        'Acrobaties'                    : '+2',
        'Art de la magie'               : '+8',
        'Artisanat (armes)'             : '+10',
        'Bluff'                         : '+4',
        'Connaissances (donjons)'       : '+0',
        'Connaissances (floklore local)': '+0',
        'Connaissances (géographie)'    : '+0',
        'Connaissances (histoire)'      : '+0',
        'Connaissances (ingénierie)'    : '+6',
        'Connaissances (mystère)'       : '+0',
        'Connaissances (nature)'        : '+0',
        'Connaissances (noblesse)'      : '+0',
        'Connaissances (plan)'          : '+6',
        'Connaissances (religion)'      : '+0',
        'Déguisement'                   : '+4',
        'Diplomatie'                    : '+4',
        'Discrétion'                    : '+7',
        'Dressage'                      : '+8',
        'Équitation'                    : '+6',
        'Escalade'                      : '-1',
        'Escamotage'                    : '+0',
        'Estimation'                    : '+2',
        'Évasion'                       : '+2',
        'Intimidation'                  : '+9',
        'Linguistique'                  : '+6',
        'Natation'                      : '+1',
        'Perception'                    : '+11',
        'Premiers secours'              : '+1',
        'Profession (marin)'            : '+8',
        'Psychologie'                   : '+1',
        'Représentation'                : '+4',
        'Sabotage'                      : '+0',
        'Survie'                        : '+1',
        'UOM'                           : '+10',
        'Vol'                           : '+2',
    }


class Kat():
    ca = '21'
    ca_touch = '13'
    ca_surprised = '18'
    saving_throw_ref = '+8'
    saving_throw_vig = '+6'
    saving_throw_vol = '+2'
    VD = '12m(8c)'
    VD_vol = '12m(8c) bonne'
    JdA_cac = '+10'
    JdA_dist = '+9'
    for_val = '19'
    dex_val = '17'
    con_val = '13'
    int_val = '7'
    sag_val = '10'
    cha_val = '11'
    size = 'N'
    raw_align = 'CB+'
    init = '+3'
    bba = '+6'
    bmo = '+10'
    dmd = '+13'
    nls = '0'
    spell_range_short = '10m(7c)'
    spell_range_med = '45m(30c)'
    spell_range_long = '180m(120c)'
    
    skill = {
        'Acrobaties'                    : '+9',
        'Art de la magie'               : '-2',
        'Artisanat'                     : '-2',
        'Bluff'                         : '+0',
        'Connaissances (donjons)'       : '-2',
        'Connaissances (floklore local)': '-2',
        'Connaissances (géographie)'    : '-2',
        'Connaissances (histoire)'      : '-2',
        'Connaissances (ingénierie)'    : '-2',
        'Connaissances (mystère)'       : '-2',
        'Connaissances (nature)'        : '-2',
        'Connaissances (noblesse)'      : '-2',
        'Connaissances (plan)'          : '-2',
        'Connaissances (religion)'      : '-2',
        'Déguisement'                   : '+0',
        'Diplomatie'                    : '+0',
        'Discrétion'                    : '+10',
        'Dressage'                      : '+0',
        'Équitation'                    : '+3',
        'Escalade'                      : '+4',
        'Escamotage'                    : '+3',
        'Estimation'                    : '-2',
        'Évasion'                       : '+3',
        'Intimidation'                  : '+0',
        'Linguistique'                  : '-2',
        'Natation'                      : '+4',
        'Perception'                    : '+10',
        'Premiers secours'              : '+0',
        'Profession (marin)'            : '+0',
        'Psychologie'                   : '+0',
        'Représentation'                : '+0',
        'Sabotage'                      : '+9',
        'Survie'                        : '+0',
        'UOM'                           : '+0',
        'Vol'                           : '+12',
    }


def build_skill_table(skill):
    data = []
    for s, v in skill.items():
        data.append((format_skill(s).replace('11', '10'), v))
    
    skill_as_table = []
    mid_table_index = int(math.ceil(len(data) / 2))
    for i in range(mid_table_index):
        sk1 = data[i]
        try:
            sk2 = data[mid_table_index + i]
            skill_as_table.append((sk1[0], sk1[1], sk2[0], sk2[1]))
        except:
            skill_as_table.append((sk1[0], sk1[1]))
    
    return ['text|%s' % build_table(skill_as_table, 13)]


def build_content_kit(perso: Perso):
    contents = [
        'section | Défense',
        'property | |<b>CA:</b> %s <b>Contact:</b>%s <b>Surpris:</b>%s <b>PV:</b> ' % (perso.ca, perso.ca_touch, perso.ca_surprised),
        'property | |<b>Réflexes:</b> %s <b>Vigueur:</b> %s <b>Volonté:</b> %s' % (perso.saving_throw_ref, perso.saving_throw_vig, perso.saving_throw_vol),
        'property | Résistances: | +2 vs %s' % SCHOOL_ICON['illusion'],
        'section | Attaque',
        'property | VD: |%s' % perso.VD,
        'property | Dist: | Arbalète légère: 1d6/19-20',
        'property| JdA: | <b>Dist:</b> %s (+1 vs reptilien & goblelin)' % perso.JdA_dist,
        'section | Sort',
        'property | NLS: |%s <b>DD:</b> 10 + lvl du sort + Cha' % perso.nls,
        'property | Porté: |%s│%s│%s' % (perso.spell_range_short, perso.spell_range_med, perso.spell_range_long),
        'property | Par jours: | ①:6 - ②:5 - ③:2',
        'text | <b>Mythique:</b>7 | <b>Pouvoir:</b>9',
        'section | Info',
        'text|%s' % build_dndstats((perso.for_val, perso.dex_val, perso.con_val, perso.int_val, perso.sag_val, perso.cha_val), False),
        'property | | <b>Taille:</b> %s <b>Align:</b> %s <b>Init:</b> %s' % (perso.size, perso.raw_align, perso.init),
        'property | Senses: | %s - <b>%s:</b> %s' % (SENSES['vision nocturne'], SKILL['Perception'], perso.skill['Perception']),
        'property | Lang: | Commun, Gnome, Profondeurs, Aquatique, Sylvestre',
    ]
    return contents


def build_content_kat(perso: Kat):
    contents = [
        'section | Défense',
        'property | |<b>CA:</b> %s <b>Contact:</b>%s <b>Surpris:</b>%s <b>PV:</b>' % (perso.ca, perso.ca_touch, perso.ca_surprised),
        'property | |<b>Réflexes:</b> %s <b>Vigueur:</b> %s <b>Volonté:</b> %s' % (perso.saving_throw_ref, perso.saving_throw_vig, perso.saving_throw_vol),
        'section | Attaque',
        'property | VD: |%s <b>Vol:</b> %s' % (perso.VD, perso.VD_vol),
        'property | CàC: | morsure: <b>1d6+4 et 1d6 %s</b>' % ENERGIES_TYPE['électricité'],
        'property | CàC: | 2x griffes: <b>1d8+4 et 1d6 %s</b>' % ENERGIES_TYPE['électricité'],
        'property| JdA: | <b>CaC:</b> %s' % perso.JdA_cac,
        'section | Info',
        'text|%s' % build_dndstats((perso.for_val, perso.dex_val, perso.con_val, perso.int_val, perso.sag_val, perso.cha_val), False),
        'property | | <b>Taille:</b> %s <b>Align:</b> %s <b>Init:</b> %s' % (perso.size, perso.raw_align, perso.init),
        'property | BBA: |%s <b>BMO:</b> %s <b>DMD:</b> %s' % (perso.bba, perso.bmo, perso.dmd),
        'property | Senses: | %s 18m(12c) - <b>%s:</b> %s' % (SENSES['vision dans le noir'], SKILL['Perception'], perso.skill['Perception']),
        "section | Dons",
        "property | Attaque en puissance : | -2 JdA & +4 dmg",
        "property | Griffes magiques : | griffes en fer froid / argent / magiques pour RD",
    ]
    return contents


def build_card_dict(name, contents, card_copyright, icon, title_size, color):
    template = {
        'count'    : 1,
        'color'    : color,
        'tags'     : [],
        'icon'     : icon,
        'title'    : name,
        'copyright': card_copyright,
        'contents' : contents,
    }
    if title_size is not None:
        template['title_size'] = title_size
    return template


def build_copyright(text):
    copy_img = '<img src="img/Pathfinder-Logo-600x257.png" style=" position: relative;    top: -6px; width: 56px;    height: 24px;"> '
    return copy_img + ('<span style="text-align: right;width: 100%%;">%s</span>' % text)


def build_cards():
    cards = []
    
    name = 'Kit'
    contents = build_content_kit(Perso())
    card_copyright = build_copyright('Kit')
    icon = 'bad-gnome'
    title_size = None
    color = 'SeaGreen'
    card = build_card_dict(name, contents, card_copyright, icon, title_size, color)
    cards.append(card)
    
    name = 'Kit - Compétences'
    contents = build_skill_table(Perso().skill)
    card_copyright = build_copyright('Kit - Compétences')
    icon = 'bad-gnome'
    title_size = None
    color = 'SeaGreen'
    card = build_card_dict(name, contents, card_copyright, icon, title_size, color)
    cards.append(card)
    
    name = 'Kit - Traits'
    contents = [
        "subtitle | Traits",
        "rule",
        "property | Présences jumelées : |  +1 Intimidation et compétence de classe. Si eidolon < 9m vous pouvez utiliser son modificateur de taille comme modificateur à vos tests d'Intimidation.",
        "property | Bénédiction de Besmara : |  Le personnage est né en pleine mer à bord d'un navire ou sur les quais d'une cité portuaire lors d'un jour de bon augure +1 aux tests de Perception et de Profession (marin). De plus 1/semaine, il peut relancer un test de marin et conserver le meilleur résultat a dire avant un test.",
    ]
    card_copyright = build_copyright('Kit - Traits')
    icon = 'bad-gnome'
    title_size = None
    color = 'SeaGreen'
    card = build_card_dict(name, contents, card_copyright, icon, title_size, color)
    cards.append(card)
    
    name = 'Kit - Dons'
    contents = [
        "subtitle | Dons",
        "rule",
        "text | <b>École renforcée (%s) :</b> DD JdS +1" % SCHOOL_ICON['invocation'],
        "text | <b>Amélioration des créatures convoquées : </b> +4 For et Con sur les créature convoquées",
        "text | <b>Convocation supérieure :</b> À chaque fois que le personnage lance un sort de convocation qui appelle plus d'une créature, il en convoque une de plus.",
        "text | <b>Métamagie : Sort éloigné :</b> Le personnage peut modifier un sort possédant une portée de type contact, courte ou moyenne en augmentant celle-ci d'une ou de plusieurs catégories.",
        "text | Les sorts dont la portée ne figure pas dans la liste donnée plus haut ne peuvent pas être affectés par ce don.",
        "text | Si le temps d’incantation normal du sort équivaut à une action simple, il faudra une action complexe pour le lancer en tant que sort de métamagie.",
    ]
    card_copyright = build_copyright('Kit - Dons')
    icon = 'bad-gnome'
    title_size = None
    color = 'SeaGreen'
    card = build_card_dict(name, contents, card_copyright, icon, title_size, color)
    cards.append(card)
    
    name = 'Kit - Mythique'
    contents = [
        "property | Grade : | 2 <b>Voie : </b> Hiérophante",
        "text|",
        "subtitle | Aptitudes mythiques de base",
        "rule",
        "bullet| Difficile à tuer (Ext)",
        "bullet| Pouvoir mythique (Sur)",
        "bullet| Montée en puissance (Sur)",
        "bullet| Excellente initiative (Ext)",
        "text|",
        "subtitle | Dons",
        "rule",
        "property | Voie duale (Archimage) : | Choisissez une voie mythique différente de celle empruntée par le personnage au moment de l’ascension. Il gagne les aptitudes du 1er grade de cette voie. Chaque fois qu’il gagne une nouvelle aptitude de voie, il peut la sélectionner dans la liste proposée par l’une ou l’autre de ses deux voies, ainsi que dans celle des aptitudes de voie universelles.",
    ]
    card_copyright = build_copyright('Kit - Mythique - Dons')
    icon = 'bad-gnome'
    title_size = None
    color = 'SeaGreen'
    card = build_card_dict(name, contents, card_copyright, icon, title_size, color)
    cards.append(card)
    
    name = 'Kit - Mythique'
    contents = [
        "subtitle | Aptitude",
        "rule",
        "property | Fureur bestiale (Sur) : | %s" % base_replace_in_desc(
            "Par une action rapide, le personnage peut dépenser une utilisation de pouvoir mythique pour octroyer à son compagnon animal, son compagnon d’armes, son eidolon, son familier ou sa monture fidèle une fraction de ce pouvoir. Par une action immédiate, la créature bénéficiaire peut se déplacer d’une distance égale ou inférieure à sa vitesse de déplacement et effectuer une attaque avec l’une de ses armes naturelles. Lorsqu’elle effectue cette attaque, vous faites deux jets pour la créature et conservez le meilleur résultat. Les éventuels dégâts infligés par cette attaque ignorent toutes les réductions des dégâts. Une créature affectée par cette aptitude peut effectuer ces actions en plus de celles qu’elle entreprend pendant son tour."),
        "property | Arcane libre (Sur) : | %s" % base_replace_in_desc(
            "Par une action rapide, le personnage peut dépenser une utilisation de pouvoir mythique pour lancer l’un de ses sorts profanes sans utiliser un sort préparé ni un emplacement de sort. Ce sort doit apparaître dans la liste des sorts profanes de la classe du personnage et ce dernier doit être d’un niveau suffisant dans cette classe de lanceur de sorts pour pouvoir le lancer. Le personnage n’a pas besoin de préparer le sort et celui-ci ne doit pas nécessairement faire partie de son répertoire de sorts connus. Lorsqu’il lance un sort de cette façon, on considère que le niveau de lanceur de sorts du personnage est augmenté de deux niveaux par rapport à son niveau réel pour déterminer les effets basés sur le niveau. Il peut appliquer les dons de métamagie qu’il maîtrise sur ce sort, mais le niveau total modifié ne peut être supérieur au sort profane de plus haut niveau qu’il est capable de lancer avec cette classe de lanceur de sorts."),
    ]
    card_copyright = build_copyright('Kit - Mythique - Aptitude')
    icon = 'bad-gnome'
    title_size = None
    color = 'SeaGreen'
    card = build_card_dict(name, contents, card_copyright, icon, title_size, color)
    cards.append(card)
    
    name = 'Kit - Mythique'
    contents = [
        "subtitle | Aptitude de voie",
        "rule",
        "property | Puissantes convocations (Sur) : | %s" % base_replace_in_desc(
            "Toutes les créatures que le personnage convoque avec ses sorts d’invocation bénéficient d’une RD 5/épique pendant toute la durée de la convocation. Si le personnage lance un sort d’invocation qui convoque plus d’une créature, il convoque une créature supplémentaire du même type. S’il lance un sort d’invocation pour convoquer une seule créature, il peut dépenser une utilisation de pouvoir mythique pour lui conférer l’archétype simple de créature mythique sauvage ou de créature mythique agile pendant toute la durée de la convocation."),
        "property | Persistance magique (Ext) : | %s" % base_replace_in_desc(
            "On considère que le niveau de lanceur de sorts du personnage est augmenté de quatre niveaux lorsqu’il s’agit de déterminer la durée de ses sorts. Cette aptitude ne modifie en rien les autres effets variables du sort."),
    ]
    card_copyright = build_copyright('Kit - Mythique - Aptitude de voie')
    icon = 'bad-gnome'
    title_size = None
    color = 'SeaGreen'
    card = build_card_dict(name, contents, card_copyright, icon, title_size, color)
    cards.append(card)
    
    name = 'Kit - Spécial'
    contents = [
        "subtitle | Gnome - Spécial",
        "rule",
        "property | Magie gnome : | 1/jour - NLS = mon LvL - DD = 10 + spell lvl + Cha",
        "text |",
        "subtitle | Conjurateur - Spécial",
        "rule",
        "property | Lien vital (Sur) : | Si Kat <0 PV, Kit peut lui passer des PV",
        "property | Sens liés (Sur) : | Kit peut, par une action simple, partager les sens de Kat. Il entend, voit, sent, goûte et touche les mêmes choses que lui. Chaque jour, il peut utiliser ce pouvoir un nombre de rounds égal à son niveau de conjurateur. Cet effet n’a pas de limite de portée mais l’eidolon et le conjurateur doivent se trouver sur le même plan. L’invocateur peut mettre un terme à ce pouvoir par une action libre.",
        "property | Protection d’allié (Ext) : | Quand Kit se trouve à portée de Kat, il reçoit un bonus de bouclier de +2 à la CA et un bonus de circonstances de +2 aux JdS. Ce bonus ne s’applique pas si l’eidolon est agrippé, sans défense, paralysé, étourdi ou inconscient.",
        "property | Appel du créateur (Sur)  : | Au niveau 6, par une AS, Kit peut appeler son eidolon à ses côtés. Ce pouvoir fonctionne comme une porte dimensionnelle qui utilise le NLS du conjurateur. Kat apparaît alors dans une case adjacente a Kit (ou aussi près que possible si les cases adjacentes sont occupées). Si Kat est hors de portée, ce pouvoir est gâché. Kit peut utiliser ce pouvoir une fois par jour au LvL 6 et une fois de plus tous les 4 LvL après celui-ci.",
    ]
    card_copyright = build_copyright('Kit - Spécial')
    icon = 'bad-gnome'
    title_size = None
    color = 'SeaGreen'
    card = build_card_dict(name, contents, card_copyright, icon, title_size, color)
    cards.append(card)
    
    name = 'Convocation de monstres IV'
    contents = [
        "subtitle | Capacité de classe",
        "rule",
        "property | Incantation | 1 AS",
        "property | Portée | Courte",
        "property | Compo | V,G,F/FD (petit sac et bougie)",
        "property | Cibles | -",
        "property | Durée | 1 min/LvL",
        "property | Jet de sauvegarde | -",
        "property | Résistance à la magie | non",
        "rule",
        "text | Invoque soit : ",
        "bullet | 1 créature de la liste de niveau IV",
        "bullet | 1d3 [+2] de la liste de niveau III",
        "bullet | 1d4+1 [+2] de la liste de niveau II et -",
    ]
    card_copyright = build_copyright('Kit - Spécial')
    icon = 'bad-gnome'
    title_size = 11
    color = 'SeaGreen'
    card = build_card_dict(name, contents, card_copyright, icon, title_size, color)
    cards.append(card)
    
    build_gnome_magic(cards)
    
    name = 'Kat'
    contents = build_content_kat(Kat())
    card_copyright = build_copyright(name)
    icon = 'feline'
    title_size = None
    color = 'DarkCyan'
    card = build_card_dict(name, contents, card_copyright, icon, title_size, color)
    cards.append(card)
    
    name = 'Kat - Compétences'
    contents = build_skill_table(Kat().skill)
    card_copyright = build_copyright('Kat - Compétences')
    icon = 'feline'
    title_size = None
    color = 'DarkCyan'
    card = build_card_dict(name, contents, card_copyright, icon, title_size, color)
    cards.append(card)
    
    name = 'Kat - Évolution'
    contents = [
        "subtitle | Évolution",
        "rule",
        "text | <b>Base:</b> Quadrupède",
        "text | <b>Réserve d'évolution:</b> 10p",
        "rule",
        "property | (1pt) Bond (Ext) : | Attaque à outrance apres une charge",
        "property | (1pt) Griffes (Ext) : | 2x attaques 1d4",
        "property | (1pt) Augmentation des dégâts (Ext) : | griffes (2x 1d6)",
        "property | (2pt) Augmentation de caractéristique (Ext) : | For +2",
        "property | (2pt) Attaques d'énergie (Sur) : | +1d6 %s sur attaques nat" % ENERGIES_TYPE['électricité'],
        "property | (2pt) Vol (Ext) : | Vol 12m(8c) bonne ",
        "property | (1pt) Monture (Ext) : | Apte à servir de monture ",
    ]
    card_copyright = build_copyright('Kat - Évolution')
    icon = 'feline'
    title_size = None
    color = 'DarkCyan'
    card = build_card_dict(name, contents, card_copyright, icon, title_size, color)
    cards.append(card)
    
    name = 'Kat - Spécial'
    contents = [
        "subtitle | Spécial",
        "rule",
        "property | Bond : | Déplacement : min 3m(2c) max VD x2 & en ligne droite dégagé. +2 JdA & -2 CA",
        "property | Esquive totale : | Si Kat est soumis à une attaque qui autorise un jet de Réflexes pour réduire les dgm de moitié, il ne subit aucun dgm s’il réussit son jet.",
        "property | Transfert des sorts  : | Kit peut lancer un sort avec pour cible « le lanceur de sorts » sur Kat (comme un sort avec une portée « contact »). Kit peut lui lancer un sort même si, normalement, il ne devrait pas affecter les créatures de type Extérieur. Les sorts ainsi lancés doivent venir de la liste de sorts du conjurateur.",
        "property | Lien  : | Kit et Kat partagent un lien mental qui leur permet de communiquer à n’importe quelle distance (tant qu’ils sont sur le même plan). ",
        "property | Dévotion : | Kat gagne un bonus de moral de +4 aux jets de Volonté contre les effets et sorts d’enchantement.",
        "text | Kit et Kat partagent les mêmes emplacements d'objets magiques.",
        "text | Si il meurs : Peux re-invoqué le lendemain (24h) avec la moitié de ses PV",
        "text | Distance max du lien :",
        "bullet | 30m-300m:pv x ½",
        "bullet | 300m-3km:pv x ¼",
        "bullet | +3km:renvoi",
    ]
    card_copyright = build_copyright('Kat - Spécial')
    icon = 'feline'
    title_size = None
    color = 'DarkCyan'
    card = build_card_dict(name, contents, card_copyright, icon, title_size, color)
    cards.append(card)
    
    name = 'Arbalète légère sous-marine'
    contents = [
        "subtitle| Attaque|<icon name=\"sword-brandish\" ></icon>",
        "rule ",
        "property |Dégâts:| 1d6 <b>Critique:</b> 19-20/×2 ",
        "property | Maniement | arme à distance",
        "property | Portée | 24m(16c) sous l'eau : 6m(4c)",
        "",
        "subtitle| Infos |<icon name=\"direction-sign\"></icon>",
        "rule ",
        "property | Prix: | 70 po <b>Poids:</b>2 kg <b>Type :</b> P",
        "property | Catégorie: | Armes courantes ",
        "property | Groupes: |arbalètes",
        "subtitle| Extra",
        "rule ",
        "text | Le rechargement de l’arbalète légère prend une action de mouvement qui provoque des attaques d’opportunité. ",
        "text | Elle tire des carreaux d’arbalète."
    ]
    card_copyright = build_copyright('Objets')
    icon = 'crossbow'
    title_size = 11
    color = 'saddlebrown'
    card = build_card_dict(name, contents, card_copyright, icon, title_size, color)
    cards.append(card)
    
    name = 'Légende - Compétences'
    contents = build_skill_help()
    card_copyright = build_copyright('Légende - Compétences')
    icon = 'rule-book'
    title_size = None
    color = 'DodgerBlue'
    card = build_card_dict(name, contents, card_copyright, icon, title_size, color)
    cards.append(card)
    
    name = 'Légende - Modificateurs'
    contents = build_mod_help()
    card_copyright = build_copyright('Légende - Modificateurs')
    icon = 'rule-book'
    title_size = None
    color = 'DodgerBlue'
    card = build_card_dict(name, contents, card_copyright, icon, title_size, color)
    cards.append(card)
    
    name = 'Légende - Archétypes mythique'
    table_data = [
        ('DV', 'Résist aux énergies dest', 'RD'),
        ('1-4', '5', '-'),
        ('5-10', '10', '5/épique'),
        ('11+', '15', '10/épique'),
    ]
    contents = [
        'section | Invincible',
        'property | Règles de reconstruction : | %s' % base_replace_in_desc(
            'CA augmentation du bonus d’armure naturelle de 2 (ou de 4 si la créature a 11 dés de vie ou plus) ; PV : +6 si DV=d6 - +8 si DV=d8; Capacités défensives elle gagne une RD et une résistance à tous les types d’énergie comme indiqué dans la table ci-contre, ainsi que blocage des attaques et second jet de sauvegarde.'),
        'section | Sauvage',
        'property | Règles de reconstruction : | %s' % base_replace_in_desc(
            'CA augmentation du bonus d’armure naturelle de 2 ; PV : +6 si DV=d6 - +8 si DV=d8; Capacités défensives elle gagne une RD et une résistance à tous les types d’énergie comme indiqué dans la table ci-contre ; Attaques spéciales toutes les attaques gagnent saignement 1 (qui se cumule avec lui-même) et sauvagerie féroce (attaque à outrance).'),
        'text | %s' % build_table(table_data, full_border=True)
    ]
    card_copyright = build_copyright('Légende - Archétypes mythique')
    icon = 'rule-book'
    title_size = 10
    color = 'DodgerBlue'
    card = build_card_dict(name, contents, card_copyright, icon, title_size, color)
    cards.append(card)
    
    return cards


def build_mod_help():
    contents = []
    mod_as_table = [
        ('1', '', '–5'),
        ('2–3', '', '–4'),
        ('4–5', '', '–3'),
        ('6–7', '', '–2'),
        ('8–9', '', '–1'),
        ('10–11', '', '+0'),
        ('12–13', '', '+1'),
        ('14–15', '', '+2'),
        ('16–17', '', '+3'),
        ('18–19', '', '+4'),
        ('20–21', '', '+5'),
        ('22–23', '', '+6'),
        ('24–25', '', '+7'),
        ('26–27', '', '+8'),
        ('28–29', '', '+9'),
        ('30–31', '', '+10'),
        ('32–33', '', '+11'),
        ('34–35', '', '+12'),
    ]
    contents.append('text | %s' % build_table(mod_as_table))
    return contents


def build_skill_help():
    contents = []
    skill_as_table = []
    j = 0
    for s, i in {v: k for k, v in SKILL.items()}.items():
        if j % 2 == 0:
            skill_as_table.append([s, i.capitalize()])
        else:
            skill_as_table[-1].append(s)
            skill_as_table[-1].append(i.capitalize())
        j += 1
    contents.append('text | %s' % build_table(skill_as_table))
    return contents


def build_gnome_magic(cards):
    spell_01 = parse_spell_page('Pathfinder-RPG.communication avec les animaux.ashx')
    spell_02 = parse_spell_page('Pathfinder-RPG.lumières dansantes.ashx')
    spell_03 = parse_spell_page('Pathfinder-RPG.prestidigitation.ashx')
    spell_04 = parse_spell_page('Pathfinder-RPG.son imaginaire.ashx')
    
    # c = build_card(spell_01.__dict__, None, icon='bad-gnome', color='SeaGreen', text_copyright='Magie gnome - 1/4')
    # cards.append(c)
    # c = build_card(spell_02.__dict__, None, icon='bad-gnome', color='SeaGreen', text_copyright='Magie gnome - 2/4')
    # cards.append(c)
    # c = build_card(spell_03.__dict__, None, icon='bad-gnome', color='SeaGreen', text_copyright='Magie gnome - 3/4')
    # cards.append(c)
    # c = build_card(spell_04.__dict__, None, icon='bad-gnome', color='SeaGreen', text_copyright='Magie gnome - 4/4')
    # cards.append(c)


def main():
    cards = build_cards()
    
    with open('out/cards-perso.json', 'w', encoding='utf-8') as fd:
        json.dump(cards, fd)


if __name__ == '__main__':
    main()
