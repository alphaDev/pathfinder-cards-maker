#!/usr/bin/python
# -*- coding: utf-8 -*-
import csv
import json
import os
import re
from enum import Enum

from common import base_replace_in_desc

ICON_SIZE = 11

SCHOOL_ICON = {
    'abjuration'   : '<span style="color:#320ceb;"><icon name="rosa-shield" size="%s" ></span>' % ICON_SIZE,
    'divination'   : '<span style="color:#8e8d91;"><icon name="crystal-ball" size="%s" ></span>' % ICON_SIZE,
    'enchantement' : '<span style="color:#f5e025;"><icon name="fairy-wand" size="%s" ></span>' % ICON_SIZE,
    'évocation'    : '<span style="color:#ad1700;"><icon name="fire-ray" size="%s" ></span>' % ICON_SIZE,
    'illusion'     : '<span style="color:#782599;"><icon name="sheikah-eye" size="%s" ></span>' % ICON_SIZE,
    'invocation'   : '<span style="color:#de7c1b;"><icon name="magic-portal" size="%s" ></span>' % ICON_SIZE,
    'nécromancie'  : '<span style="color:#0a0a0a;"><icon name="burning-skull" size="%s" ></span>' % ICON_SIZE,
    'transmutation': '<span style="color:#007000;"><icon name="materials-science" size="%s" ></span>' % ICON_SIZE,
    'universel'    : '<span style="  background-image: linear-gradient(to left, violet, indigo, blue, green, yellow, orange, red);   -webkit-background-clip: text;  color: transparent;"><icon name="ringed-planet" size="%s" ></span>' % ICON_SIZE,
}


def build_card_dict(name, contents, level, card_copyright, title_size=None, icon='', color=''):
    if not icon:
        icon = 'white-book' if level is None else 'white-book-%s' % level
    if not color:
        color = 'SandyBrown'
    template = {
        'count'    : 1,
        'color'    : color,
        'tags'     : ['lvl-%s' % level],
        'icon'     : icon,
        'title'    : name,
        'copyright': card_copyright,
        'contents' : contents,
    }
    if title_size is not None:
        template['title_size'] = title_size
    return template


class PjClass(Enum):
    Conj = 'conj'
    Pre = 'pre'
    Mag = 'mag'


my_pj_class = PjClass.Conj


def read_spell_file():
    spells = []
    with open('out/spells-%s.csv' % my_pj_class.name, 'r', encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            spells.append(row)
    
    return spells


def format_class_in_level(raw_class):
    if raw_class.lower() in ('conj', 'conjurateur', 'con'):
        return PjClass.Conj
    if raw_class.lower() in ('prê', 'prêtre'):
        return PjClass.Pre
    if raw_class.lower() in ('magus', 'magu', 'mgs'):
        return PjClass.Mag
    return None


def get_level(spell, pj_class):
    try:
        raw_level = spell['level']
    except:
        raw_level = spell.level
    
    raw_by_class = raw_level.split(',')
    default = 0
    for r in raw_by_class:
        sp = r.split()
        _pj_class = format_class_in_level(sp[0])
        default = int(sp[1])
        if _pj_class == pj_class:
            return default
    print(raw_by_class)
    return default


def format_components(spell):
    components = spell['components']
    components = components.replace('une ', '').replace('un ', '')
    components = components.replace('bout de papier avec le nom de la cible', 'nom sur un papier')
    components = components.replace('petit morceau ', 'morceau ')
    components = components.replace('centimètres ', 'cm ')
    components = components.replace('tentacule de pieuvre ou de calmar ', 'tentacule')
    return components


def format_casting_time(spell):
    casting_time = spell['casting_time']
    return casting_time.lower().replace('action simple', 'AS')


def format_range(spell):
    _range = spell['range']
    if _range.lower().startswith('courte'):
        return 'courte'
    if _range.lower().startswith('moyenne'):
        return 'moyenne'
    if _range.lower().startswith('longue'):
        return 'longue'
    
    _range = _range.replace('niveau', 'LvL')
    _range = _range.replace('courte (7,50 m + 1,50 m/2 niveaux) (5 c + 1 c/2 niveaux)', 'courte')
    return _range


def format_area_effect(spell):
    area_effect = spell['area_effect']
    if area_effect == 'émanation en forme de cône':
        return 'cône'
    if area_effect == 'émanation de 6 m (4 c) de rayon autour du point choisi':
        return 'rayon 6m (4c)'
    return area_effect


def format_duration(spell):
    duration = spell['duration']
    if duration == 'concentration, jusqu\'à 1 minute/niveau (T)':
        return 'concentré pour 1min/lvl'
    
    duration = duration.replace('round/niveau', 'rd/lvl')
    duration = duration.replace('minutes', 'min')
    duration = duration.replace('niveau', 'LvL')
    duration = duration.replace('personnage', 'perso')
    return duration


def format_description(spell, contents):
    name = spell['name']
    description = spell['description']
    
    description = description.replace('etc.)', 'etc)')
    
    if name == 'Détection de la magie':
        description = '''
        {BULLET} Round 1 : Présence ou absence d’auras magiques.
        {BULLET} Round 2 : Nombre d’auras magiques et intensité de la plus puissante.
        {BULLET} Round 3 : Intensité et emplacement précis de chaque aura.
        Identifier un objet : Test Art de la magie (DD: 15 + NLS de l’objet magique).
        Identifier l\'école de magie : Dois voir l\'aura, test de connaissances (mystères) un test par aura (DD: 15 + sort lvl, ou 15 + NLS/2 si l\'effet n\'est pas un sort).
        Le sort est bloqué par 30 cm de pierre, 2,5 cm de métal, une mince feuille de plomb ou 90 cm de bois ou de terre.
        Il est possible d’user de permanence sur détection de la magie.
         '''
    
    if name == 'Alarme':
        description = '''
        Alarme fait résonner une sonnerie d\'alerte audible ou mentale chaque fois qu\'une créature au minimum de taille TP pénètre ou touche la zone protégée.
        Les créatures qui donne le mdp (choisi par le personnage au moment de l\'incantation), astrales ou éthérées ne déclenchent pas l\'alarme.
        L\'alarme est entendu meme sous le sort de silence.
        {EMPTYLINE}.
        {EMPTYLINE}.
        {SECTION} Alarme mentale.
        Le personnage est le seul alarmé part une brève sonnerie, mais il doit se trouver à moins de 1,5 km.
        Il est réveillé s\'il dormait mais ne dérange pas sa concentration.
        {SECTION} Alarme audible.
        Un son de cloche retentit pendant 1 round à 18m - réduite de 3m par porte close et 6m par mur épais (54m si environnement silencieux).
        Il est possible d\'user de permanence sur un sort d\'alarme.
        '''
    
    if name == 'Lumière':
        description = description.replace('de plus (les ténèbres se muent en faible lumière et cette dernière en lumière normale).', '.')
    
    if name == 'Armure de mage':
        description = description.replace('elles le font pour les armures physiques.', 'elles le font pour les armures physiques.{EMPTYLINE}.')
    
    if name == 'Message':
        description = '''
        Le sort permet de chuchoter des messages.
        Les personnes alentour discernent le message si elles réussissent un test de Perception DD 25.
        Le personnage doit cibler du doigt toutes les créatures à inclure dans le sort.
        Dès qu'il murmure, le message est entendu par tous les clibles à porté.
        L'effet du sort est bloqué par un silence d'origine magique, 30 cm de pierre, 2,5 cm de métal, une feuille de plomb, ou 90 cm de bois ou de terre.
        Le message peut contourner un obstacle physique, à condition que le chemin emprunté reste à l'intérieur des limites de portée.
        Les cibles recevant le message peuvent chuchoter une réponse au personnage.
        Le sort transmet que les sons, il ne permet pas de franchir la barrière du langage.
        '''
    
    if name == 'Signature magique':
        description = '''
        Ce sort permet au personnage d'apposer sa rune : signature/marque personnelle de 6 caractères distincts au plus.
        La rune peut être visible ou non.
        La rune s'inscrit dans n'importe quel matériau sans abîmer l'objet.
        Si la rune est invisible, détection de la magie, détection de l'invisibilité, vision lucide, une gemme de vision ou une robe de vision totale la rend visible (mais elle ne devient pas forcément compréhensible pour autant).
        Lecture de la magie révèle les mots contenus dans la rune.
        Seul le créateur ou le sort effacement permet de dissiper la rune.
        Si on la trace sur un être vivant, la rune s'efface peu à peu et disparaît au bout d'un mois environ.
        Il faut apposer sa signature magique sur un objet avant de lancer invocation instantanée dessus.
        '''
    
    if name == 'Agrandissement':
        description = '''
        Ce sort agrandis la cible instantanément : taille x2, poids x8, taille cat +1, +2 For, -2 Dex (1 min), -1 JdA, -1 CA lié a taille, la VD n'est pas modifiée, espace occupé & allonge nat +3m.
        Si pas asser de place, la cible grandis au max, puis peut faire un test de For (avec la nouvelle valeur) pour briser toute entrave, si échec : bloquée mais pas blessée.
        Tout ce que porte la cible grandit avec elle, propriétés magiques restent intactes, les armes de CàC font + de dgm.
        1d3>1d4>1d6>1d8>2d6>3d6 / 1d10>2d8>3d8 / 1d12>3d6 / 2d10>4d8 / 2d4>2d6 / 2d10>4d8.
        Les objets agrandis lachés recouvrent taille normale, les armes de jet et les projectiles : dgm normaux.
        Les effets magiques augmentant la taille du sujet ne sont pas cumulatifs, agrandissement contre et dissipe rapetissement.
        Il est possible d'user de permanence.
        '''
    
    if name == 'Agrandissement de groupe':
        description = '''Ce sort est similaire à agrandissement, si ce n’est qu’il affecte plusieurs créatures.'''
    
    if name == 'Rapetissement':
        description = '''
        Ce sort rapetisse la cible instantanément : taille ½, poids ⅛, taille cat -1, -2 For (1 min), +2 Dex, +1 JdA, +1 CA lié a taille, la VD n'est pas modifiée, espace occupé & allonge nat -3m.
        Tout ce que porte la cible rapetisse avec elle, propriétés magiques restent intactes, les armes de CàC font - de dgm.
        1d3>1d4>1d6>1d8>2d6>3d6 / 1d10>2d8>3d8 / 1d12>3d6 / 2d10>4d8 / 2d4>2d6 / 2d10>4d8.
        Les objets rapetisse lachés recouvrent taille normale, les armes de jet et les projectiles : dgm normaux.
        Les effets magiques réduisant la taille du sujet ne sont pas cumulatifs, rapetissement contre et dissipe agrandissement.
        Il est possible d'user de permanence.
        '''
    
    if name == 'Bouche magique':
        description = '''
        Fait apparaître sur la cible une bouche enchantée qui délivre un message si les conditions spécifié lors de l'incantation se produit; Le message doit contenir 25, prendre 10min max et être dans une langue parlée par le personnage.
        La bouche remue les lèvres en fonction de ce qu'elle dit mais ne peut prononcer ni incantation, ni mot de commande, ni activer le moindre effet magique.
        Les conditions peuvent être simples ou complexes mais sont purement visuelles ou auditives.
        Condition visuelle : les sorts ténèbres et invisibilité empêche mais pas l'obscurité naturelle ; Conditions auditives : Discrétion et sorts de silence empêche.
        Peut être abusée par un déguisement ou une illusion.
        La portée max des condition est de 4,50m (3c) par lvl ; Mais ne répond qu'aux stimuli à portée de vue ou d'ouïe.
        Il est possible d'user de permanence.
        '''
    
    if name == 'Bouclier de pierre':
        description = '''
        Un bloc de pierre de 2,5cm d'épaisseur jaillit du sol, s'interposant entre le personnage et l'adversaire choisi.
        Ce bouclier de pierre offre au personnage un abri contre cet ennemi jusqu'au début de son prochain tour, lui accordant un bonus de +4 à la CA et un bonus de +2 à ses jets de Réflexes.
        Si l'adversaire rate ses attaques de 4 ou moins, elles frappent le bouclier.
        Le bouclier de pierre a une solidité de 8 et 15 points de vie ; S'il est détruit, le sort se dissipe et le bouclier s'effondre et disparaît.
        Les sorts et les effets endommageant la zone infligent des dégâts au bouclier.
        Le lanceur ne peut pas utiliser ce sort s'il n'est pas adjacent à une grande zone de terre ou de pierre.
        Le moine qinggong peut choisir ce sort comme pouvoir de ki au niveau 4 coûtant 1 point de ki à activer.
        '''
    
    if name == 'Convocation de monstres mineurs':
        description = '''
        Ce sort fonctionne comme convocation de monstres I mais le personnage ne peut invoquer que 1d3 animaux TP ou plus petits comme les chauves-souris, les lézards, les singes, les rats, les corbeaux, les crapauds ou les belettes.
        Les animaux invoqués doivent tous être de la même sorte.
        Comme pour les animaux invoqués avec convocation de monstres I, le personnage peut appliquer un archétype d’alignement aux animaux.
        '''
    
    elif name.startswith('Convocation de monstres'):
        description = '''
        Cette incantation invoque une créature (voir effet) à l’endroit choisi par le personnage, agit immédiatement, pendant le tour du personnage et se bat avec tous ses pouvoirs.
        Si le personnage peut communiquer avec elle, il est possible de lui demander de s’abstenir d’attaquer, de cibler certaines créatures, ou d’obéir à d’autres ordres.
        Les créature ne peuvent convoquer d’autres créatures, user de ses facultés de téléportation ou de déplacement planaire, ni utiliser de sort ni de pouvoir magique qui imitent des sorts avec composantes matérielles onéreuses.
        Une créature ne peut pas être convoquée dans un environnement qui ne saurait assurer sa survie.
        Si les créatures n'ont pas d'alignement spécifique, elles prennent l'alignement du personnage.
        '''
    
    if name == 'Endurance aux énergies destructives':
        description = '''
        Le cible ne souffre pas de la chaleur ou du froid lorsqu'elle se trouve dans un environnement extrême.
        Elle se sent à son aise par des températures allant de -45°C à +60°C et ne doit pas effectuer le moindre jet de Vigueur dans ces conditions; Son équipement est aussi protégé.
        Le sort n'offre aucune protection contre les dégâts de feu et de froid ni contre les autres dangers liés à l'environnement comme la fumée ou le manque d'air par exemple.
        '''
    
    if name == 'Feuille morte':
        description = '''
        Feuille morte affecte des créatures de taille M ou moins avec leur équipement & objets, jusqu'à leur charge maximale ou des objets de taille équivalente.
        Si cible de taille G elle compte comme 2 créatures de taille M.
        Les cibles doivent etre en chute libre, il a aucun effet sur les projectiles, coup d'épée, créature qui charge ou vole.
        Si cible un objet qui tombe, les dgm dépendant uniquement de la masse divisés par 2.
        '''
    
    if name == 'Graisse':
        description = '''
        Ce sort recouvre une surface solide d'une couche de graisse glissante.
        Au lancement jet de Réflexes pour éviter de tomber, pour se déplacer à ½ VD : test d'Acrobaties de DD 10, si echec >5 bouge pas et jet de Réflexes pour éviter de tomber, si echec <5 tombe.
        Sur un object, si manié ou porté jet de Réflexes, en cas d'échec, l'objet lui glisse hors des mains, sinon il est graissé, pour ramasser ou d'utiliser un objet graissé : jet de sauvegarde.
        Donne un bonus de circonstances de +10 aux tests d'Évasion et aux tests de manœuvre offensive visant à échapper à une lutte, ainsi qu'au DMD pour éviter de se faire agripper.
        '''
    
    if name == 'Libération':
        description = '''
        Ce sort brise le lien entre le personnage et son eidolon, ce qui permet à ce dernier de s'aventurer à plus de 30 mètres (20 c) de lui sans malus.
        Il peut parcourir n'importe quelle distance tant que le sort est actif, lorsque le sort expire eidolon perd les points de vie appropriés en fonction de son éloignement et il est possible qu'il soit renvoyé dans son plan d'origine.
        Tant que le sort fait effet, le personnage ne peut pas sacrifier de points de vie pour empêcher que son eidolon ne reçoive de dégâts.
        Le transfert de dégâts par le lien vital est temporairement impossible.
        Si le personnage essaye de se servir du pouvoir de transposition alors que ce sort fait encore effet, il doit lancer un dé sur la table des incidents de téléportation en utilisant la ligne "soigneusement étudié".
        '''
    
    if name == 'Sceau de colle':
        description = '''
        Sur une zone : au lancement les créatures dans la zone doivent réussir un jet de Réflexes sinon elles sont enchevêtrées, pour se libérer test de manœuvre offensive ou un test d'Évasion par une AS contre le DD de ce sort.
        La zone est considérée comme un environnement difficile.
        Une créature qui traverse la zone doit réussir un test de manœuvre offensive ou un test d'Évasion DD du sort sinon elles s'arrêtent et sont enchevêtrées.
        Sur un object : la créature qui tient l'objet ciblé doit réussir un jet de Réflexes contre le DD du sort pour que l'object ne soit pas affecté, sinon l'objet reste collé.
        Pour séparer 2 objets collés, il faut réussir un test de manœuvre offensive ou un test de Force par une action de mouvement (DD du sort).
        Si armures et vêtements collants -> malus de circonstances de -10 aux tests d'Évasion et de manœuvre offensive vs étreinte & au DMD du porteur pour éviter d'être agrippé.
        '''
    
    if name == 'Serviteur invisible':
        description = '''
        Il peut aller chercher des objets, ouvrir des portes (normal et pas coincées), tirer une chaise, faire le ménage, etc.
        Il n'exécute qu'une seule tâche à la fois et la répète jusqu'à ce qu'on lui dise de faire autre chose.
        Sa Force est de 2, il peut déclencher si il suffit de leur appliquer une pression de 10 kilos.
        Il est incapable de réaliser des tâches qui nécessitent un test de compétence avec d'un DD de 10+ ou d'un test de compétence nécessitant une formation.
        Sa VD est de 4,50 m et il ne peut ni voler, ni escalader, ni nager, il peut marcher sur l'eau.
        Le serviteur est incapable de combattre, Il est impossible de le tuer mais il se dissipe si des attaques de zone lui infligent un total de 6 dgm (il a le droit a aucun JdS).
        Il cesse d'exister dès que son créateur tente de l'envoyer en dehors des limites de portée du sort.
        '''
    
    if name == 'Mur de vent':
        description = description.replace('Il est possible de le créer cylindrique ou carré pour délimiter une zone bien précise.', '')
    
    if name.startswith('Protection contre'):
        replaced = [
            (r'''Ce sort protège son bénéficiaire contre les attaques des créatures d'alignement \w.*, mais aussi contre le contrôle mental et les créatures convoquées.''', ''''''),
            ('''Il crée une barrière magique à trente centimètres autour du sujet.''', ''''''),
            ('''Cette barrière se déplace avec le personnage et possède trois effets principaux :''', ''''''),
            ('''Ces deux bonus s'appliquent uniquement contre les attaques portées par les créatures''', ''' vs les créatures'''),
            (r'''la cible peut immédiatement effectuer un nouveau jet de sauvegarde contre tous les sorts et les effets de possession ou de contrôle mental qui l'affectent \(et qui autorisent un jet de sauvegarde\).''',
             '''Vs les effets de contrôle mental : nouveau jet de sauvegarde'''),
            (r'''Parmi ces effets pour lesquels la cible bénéficie d'un nouveau jet de sauvegarde, on trouve les effets d'enchantement \(charme\) et d'enchantement \(coercition\).''', ''''''),
            ('''Ces nouveaux jets de sauvegarde sont effectués''', ''''''),
            ('''tels que charme-personne, injonction et domination.''', ''''''),
            ('''DD du premier jet de sauvegarde.''', '''DD du premier jet de sauvegarde, '''),
            (r'''En cas de réussite, les effets en question sont supprimés pendant toute la durée de la protection contre \w.* \w.* mais ils reprennent cours dès que la protection expire.''', ''''''),
            (r'''Tant que la cible est affectée par la protection contre \w.* \w.*, elle est immunisée contre toutes les nouvelles tentatives de la posséder ou d'exercer un contrôle mental sur elle.''',
             '''si reussite : immuniser le temps du sort.'''),
            (r'''Ce second effet ne fonctionne que contre les sorts et les effets créés par des objets ou des créatures \w.* \(à l'appréciation du MJ\).''', ''''''),
            (r'''Le sort n'expulse pas les forces à l'origine de ce contrôle \(comme un fantôme ou un lanceur de sort lançant possession\) mais il les empêche de contrôler la cible.''', '''La cible est encore possedé mais plus contrôler.'''),
            ('''Enfin, le sort empêche les créatures convoquées de toucher le personnage.''', ''''''),
            ('''Les attaques naturelles de ces créatures échouent automatiquement et elles sont obligées de reculer si leurs attaques les obligent à toucher l'individu protégé. ''',
             '''Les attaques nat des créatures convoquées & aligné échouent automatiquement et font reculer,'''),
            (r'''Les créatures d'alignement autre que \w.* sont immunisées contre cet effet du sort. ''', ''''''),
            ('''La protection contre les créatures convoquées s'achève instantanément si le sujet attaque l'entité ou tente de la repousser à l'aide de la barrière.''',
             ''' cette protection s'achève si la cible tente de repousser avec la barrière, '''),
            ('''Enfin, la résistance à la magie peut permettre à une créature de toucher le personnage.''', ''' la RM permet de touché la cible.'''),
            (
                r'''Ce sort .* à protection contre \w* \w* mais les bonus de parade et de résistance s'appliquent aux attaques \w* par les créatures \w.* et les créatures convoquées d'alignement \w.* ne peuvent pas toucher le bénéficiaire.''',
                ''''''),
            ('''Premièrement, l''', '''L'''),
            ('''Deuxièmement, ''', ''''''),
            ('''e personnage''', '''a cible'''),
            ('''jets de sauvegarde''', '''JdS'''),
            ('''jet de sauvegarde''', '''JdS'''),
            (r'''\+2 aux JdS.''', '''+2 aux JdS, '''),
            ('''contrôle mental''', '''ctrl mental'''),
            ('''contre''', '''vs'''),
        ]
        
        for r in replaced:
            description = re.sub(*r, description)
    
    if name == 'Abri de toile':
        description = description.replace('ce qui permet de la camoufler.', 'ce qui permet de la camoufler,')
        description = description.replace('créatures I', 'créatures')
    
    if name == 'Coursier fantôme':
        description = '''
        Cette invocation fait apparaître une créature quasi-réelle ayant la forme d'un cheval de taille G, que seul le personnage peut chevaucher.
        Il a une tête et un corps noirs, des crins gris, et des sabots translucides et intangibles qui ne font pas le moindre bruit, il est harnaché de ce qui s'apparente à une selle, un mors et des rênes, il ne combat jamais, mais terrifie les animaux normaux, qui ne peuvent pas l'attaquer.
        Il a une CA de 18 (-1 taille, +4 arm nat, +5 Dex) et 7 points de vie +1 x niveau de lanceur de sorts, se dissipe à 0 points de vie.
        Sa VD est de 6m x ½ NLS, (max 30m au lvl 10) & peut transporter son cavalier + 5kg par niveau de lanceur de sorts.
        Il hérite de pouvoirs en fonction du niveau de lanceur de sorts.
        {BULLET} Niveau 8 : le cheval peut avancer dans le sable, la boue ou les marécages sans que sa vitesse de déplacement n'en soit affectée.
        {BULLET} Niveau 10 : marche sur l'onde à volonté.
        {BULLET} Niveau 12 : marche dans les airs à volonté.
        {BULLET} Niveau 14 : vol avec la même vitesse de déplacement & un bonus au Vol égal au niveau de lanceur de sorts.
        '''
    
    if name == 'Création de fosse':
        description = '''
        Le personnage crée une fosse extradimensionnelle de 3m (2c) x 3m (2c).
        Sa profondeur est de 3 mètres (2 c) par tranche de deux niveaux de lanceur de sorts (maximum 9 mètres (6 c)).
        Il doit créer cette fosse sur une surface horizontale d'une taille suffisante.
        Elle ne pèse rien et ne déplace pas le matériau qui se trouve en dessous.
        Toute créature qui se trouve dans la zone où il invoque la fosse doit réussir un jet de Réflexes pour ne pas tomber dedans.
        De plus, les bords sont en pente et toute créature qui termine son tour sur une case adjacente à la fosse doit faire un jet de Réflexes avec un bonus de +2 pour ne pas glisser dedans.
        Les créatures soumises à un effet ayant pour but de les faire tomber dans la fosse (comme une bousculade) n'ont pas droit à un jet de sauvegarde pour éviter la chute si elle sont affectées par un effet qui les repousse.
        Les créatures qui tombent dans la fosse subissent des dégâts de chute normaux.
        Les parois de pierre brute de la fosse ont un DD d'Escalade de 25.
        Quand le sort se termine, les créatures qui se trouvent dans le trou remontent avec le fond de la fosse jusqu'à la surface, le tout en un seul round.
        '''
    
    if name == 'Détection de pensées':
        description = '''
        Grâce à détection de pensées, le lanceur de sorts peut détecter les pensées superficielles des créatures situées dans la zone d'effet.
        Ce sort ne révèle pas l'emplacement des créatures que le personnage ne voit pas.
        {EMPTYLINE}.
        {BULLET} Round 1 : présence ou absence de pensées (de créatures conscientes ayant une valeur d'Intelligence ≥ 1).
        {BULLET} Round 2 : nb esprits conscients et la valeur d'Intelligence de chacun d'eux, si une créature possède une valeur d'Intelligence ≥ 26 et au moins +10 que celle du personnage, celui-ci est étourdi pendant 1 round et le sort se termine.
        {BULLET} Round 3 : pensées superficielles de tous les esprits situés dans la zone d'effet, si une créature réussit un jet de Volonté, le personnage ne peut pas en lire ses pensées ; il doit lancer détection de pensées une seconde fois s'il veut une autre chance de le faire.
        Le personnage peut pivoter sur lui-même pour examiner une nouvelle zone chaque round.
        Le sort fonctionne à travers les barrières si celles-ci ne sont pas trop épaisses : il est bloqué par 30 cm de pierre, 2,5 cm de métal, une mince feuille de plomb ou 90 cm de bois ou de terre.
        '''
    
    if name == 'Résistance aux énergies destructives':
        description = '''
        Ce sort offre une protection limitée contre la forme d'énergie choisie : acide, électricité, feu, froid ou son.
        Le sujet bénéficie d'une résistance de 10 pt contre ce type d'énergie, ce qui signifie que chaque fois qu'il subit des dégâts (que leur source soit d'origine naturelle ou magique),
        ils sont réduits de 10pt avant d'être décomptés de ses points de vie restants, cette résistance passe à 20pt au niveau 7 et à 30pt (le maximum) au niveau 11.
        Le sort protège également l'équipement du bénéficiaire.
        Il absorbe seulement les dégâts, ne bloque pas les effets secondaires indésirables.
        Si un personnage bénéficie de protection contre les énergies destructives et de ce sort, la protection agit seule jusqu'à ce que son potentiel d'absorption soit épuisé.
        '''
    
    if name == 'Rapidité':
        description = '''
        Le sujet se déplace et agit plus rapidement que d'habitude.
        Quand il entame une attaque à outrance +1 attaque, il utilise son bonus de base à l'attaque maximal, auquel il ajoute tous les modificateurs adéquats.
        Cet effet ne se cumule pas avec un autre de même type, comme une arme rapide.
        Le sort n'offre pas une action supplémentaire à proprement parler, Il ne permet donc pas de lancer un second sort.
        La créature bénéficie d'un bonus de +1 aux jets d'attaque, d'un bonus d'esquive de +1 à la CA et aux jets de Réflexes, elle perd ce bonus d'esquive si elle perd son bonus de Dex à la CA, VD +9m (max x2 VD normal), il s'agit d'un bonus d'altération.
        On ne peut pas cumuler plusieurs effets de rapidité, Rapidité contre et dissipe lenteur.
        '''
    
    if name == 'Fouet d\'araignées':
        description = '''
        Le perso convoque des 100aine d'araignées de taille Min, agglutinées de la forme d'un fouet adapté à sa taille.
        Il peut manier cet objet comme un fouet, qui fait des attaques de contact au corps à corps et fait des dégâts de nuée d'araignées (1d6 points de dégâts + poison et distraction).
        Il ne peut pas servir pour faire des attaques de bousculade, d'étreinte ou de croc-en-jambe et peut rater en cas de camouflage et d'abri.
        Il est immunisé contre tous les dégâts d'arme, il ne peut pas être chancelant ou réduit à un état mourant à cause de dégâts, et il est immunisé contre tous les sorts ou effets ciblant un nombre spécifique de créatures.
        Il reçoit 50% de dégâts en + des sorts ou effets affectant une zone mais, que si le personnage rate un jet de sauvegarde sur un 1 nat ou si le fouet est pris pour cible, et n'a pas la sensibilité des nuées aux vents importants.
        Par une action simple, le personnage peut transformer son fouet en une nuée d'araignées (toutes les cases de la nuée doivent être dans un rayon de 4,5 mètres autour du personnage).
        La nuée a 3 points de vie et conserve cette forme pendant 2 rd ou jusqu'à la fin du sort (ce qui advient en premier).
        '''
    
    if name == 'Lévitation':
        description = '''
        Par une action de mouvement, le personnage dirige mentalement la cible, dans une limite de 6m (4c) par round, verticalement (horizontalement impossible).
        La cible doit être consentante, l'objet ne doit pas être tenue ni appartenir à une créature non consentant.
        La cible peut se déplacer latéralement en s'aidant d'une paroi ou d'un plafond (dans ce cas ½ vitesse de déplacement).
        Si la cible essaye de se battre (avec des armes de corps à corps ou à distance), elle est victime d'instabilité : malus de -1 par attaques (maximum -5), stabilisation : 1rd complet, puis rependre à -1.
        '''
    
    if name == 'Invisibilité':
        description = '''
        La créature et son équipement ou l'objet touché devient invisible.
        Une créature invisible peut camoufler un object, toute partie à plus de 3m(2c) d'elle est visible.
        La lumière ne disparaît jamais, même si sa source est invisible.
        Une créature invisible et immobile bénéficie d'un bonus de +40 aux tests de Discrétion, +20 si déplacement.
        Le sort prend fin si la créature attaque un ennemi, ne casse pas le sort : les actions qui causent du tort indirectement, les sorts qui cible les alliés, les actions contre des objets que personne n'utilise ou ne porte.
        Il est possible d'user de permanence sur un sort d'invisibilité affectant des objets.
        '''
    
    if name == 'Son imaginaire':
        description = description.replace('On peut rendre le son imaginaire permanent à l\'aide d\'un sort de permanence.', 'Il est possible d\'user de permanence sur un sort de son imaginaire.')
    
    if name == 'Prestidigitation':
        description = description.replace(
            'Par exemple, le personnage peut, une fois par round, soulever très lentement un objet ne pesant pas plus de cinq cents grammes, colorier, nettoyer ou salir un objet ne faisant pas plus de trente centimètres de côté, mais aussi chauffer, refroidir ou donner du goût à cinq cents grammes de matière inerte.',
            'Par exemple, le personnage peut, une fois par rd, soulever très lentement un objet de au plus 500g, colorier, nettoyer ou salir un objet de au plus de 30cm de côté, mais aussi chauffer, refroidir ou donner du goût à 500g de matière inerte.')
    
    if name == 'Bouclier de feu':
        description = '''
        Ce sort cerne le personnage d’un rideau de flammes qui inflige des dégâts à quiconque le touche au corps à corps.
        Les flammes le protègent également des attaques à base de froid ou de feu, le choix devant être fait au moment de l’incantation.
        Toute créature touchant le lanceur de sorts à l’aide d’une arme naturelle ou de corps à corps lui cause des dégâts normaux, mais elle perd 1d6 points de vie, +1 par niveau de lanceur de sorts (+15 maximum), ce sont des dégâts de feu ou de froid.
        La résistance à la magie de la créature la protège normalement contre cette attaque.
        Les armes avec une allonge exceptionll, protege du bouclier de feu.
        Bouclier chaud: Les flammes sont chaudes au toucher; Le personnage encaisse des dégâts /2 contre les attaques de froid; Si ces attaques autorisent un jet de Réflexes pour dégâts/2, le personnage y échappe totalement en cas de jet réussi.
        Bouclier froid: Les flammes sont froides et protègent contre les attaques à base de feu (dégâts /2); Si les attaques autorisent un jet de Réflexes pour dégâts/2, le personnage y échappe totalement en cas de jet réussi.
        '''
    
    if name == 'Convocation de créature totémique':
        description = '''
        Le personnage doit avoir été élevé par les claniques et être considéré comme faisant partie du clan pour être en mesure de lancer ce sort.
        Les personnages ayant accès à ce sort ne peuvent convoquer que des créatures vénérées par le clan auquel ils appartiennent, comme indiqué dans la liste qui suit.
        Aux exceptions indiquées ci-dessus, ce sort fonctionne comme convocation d'alliés naturels III.
        Clan de la Lune: Elémentaire de l'Air (taille P), 1d3 chauves-souris, loup, 1d3 chouettes.
        Clan de la Hache: 1d3 aigles, élémentaire de la Terre (taille P), élémentaire de l'Eau (taille P).
        Clan de l'Aigle: Élémentaire de l'Air (taille P), 1d3+1 aigles (familiers), cheval.
        Clan de la Lance: Élémentaire de l'Air (taille P), élémentaire de la Terre (taille P), 1d3 aigles.
        Clan du Soleil: 1d3 punaises de feu, élémentaire du Feu (taille P), cheval.
        Clan du Crâne: 1d3+1 aigles (vautours), 1d3 punaises de feu, 1d3 mille-pattes géants.
        Clan du Vent: Élémentaire de l'Air (taille P), 1d3 aigles, élémentaire de la Terre (taille P).
        '''
    
    if name == 'Rapetissement de groupe':
        description = '''Ce sort est semblable à rapetissement, si ce n’est qu’il affecte plusieurs créatures.'''
    
    if name == 'Tentacules noirs':
        description = '''
        Cette incantation fait apparaître un amas de tentacules noirs qui semblent surgir du sol (ou de la surface de l'eau).
        Ils s'agrippent et s'enroulent autour des créatures qui entrent dans la zone et les immobilisent afin de les écraser.
        À chaque round, au début du tour du personnage et lors du round d'incantation, toutes les créatures dans la zone d'effet du sort sont victimes d'une manœuvre de combat pour les agripper.
        Les créatures qui entrent dans la zone d'effet sont immédiatement attaquées, elles ne provoquent pas d'attaque d'opportunité.
        Elles utilisent le niveau de lanceur de sorts du personnage comme bonus de base à l'attaque pour déterminer leur BMO et reçoivent un bonus de Force de +4 et un bonus de taille de +1.
        À chaque round, le personnage lance une seule fois les dés pour la totalité des effets du sort.
        Lorsqu'elles réussissent un test de lutte, elles infligent 1d6+4 points de dégâts et leur victime est en lutte et ne ne peut rien faire tant qu'elle est agrippée.
        Elles reçoivent un bonus de +5 au test de lutte contre les adversaires qu'elles agrippent.
        À chaque fois qu'elles réussissent un test de lutte, elles infligent 1d6+4 points de dégâts.
        Le DMD pour échapper à leurs étreinte est égale à 10 + leur BMO.
        On ne peut pas les blesser mais on peut les dissiper, la zone qu'elles affectent est un terrain difficile.'''
    
    if name == 'Possession de marionnette':
        description = '''
        Le personnage projette son âme hors de son corps, dans celui d'une créature consentante.
        La protection contre le mal et autres sceaux similaires bloque ce transfert; L'âme de la cible partage son corps avec le personnage, elle est incapable de faire quoi que ce soit mais elle peut tout même utiliser ses sens; Le personnage et la cible peuvent communiquer par télépathie sans barriere du langage.
        Le personnage conserve son Intelligence, sa Sagesse, son Charisme, son niveau, sa classe, son bonus de base à l'attaque, son bonus aux jets de sauvegarde, son alignement et ses capacités mentales.
        Le corps conserve sa Force, sa Dextérité, sa Constitution, ses points de vie, ses capacités naturelles et ses attaques naturelles.
        Un corps avec des membres supplémentaires ne permet pas au personnage de faire des attaques en plus de la normale ni plus avantageuses à deux armes; Le personnage ne peut pas activer les pouvoirs magiques, surnaturels ou extraordinaires de la cible.
        Le personnage peut retourner dans son corps par une action simple, ce qui met un terme au sort.
        Tant que l'âme du personnage possède la cible, son propre corps est sans défense.
        Si le corps hôte se fait tuer, le personnage retourne dans son propre corps s'il est à portée et la force vitale de l'hôte se dissipe (ce qui se solde par sa mort).
        Si le corps hôte se fait tuer alors que le corps du personnage est hors de portée, le personnage meurt comme son hôte.
        Toute force vitale qui n'a pas d'endroit où aller est considérée comme morte.
        '''
    
    if name == 'Porte dimensionnelle':
        description = '''
        Cette incantation permet de se déplacer instantanément jusqu'à un point situé à portée.
        Le personnage choisi l'endroit en le visualisant ou avec ses coordonnées.
        Après avoir utilisé ce sort, le personnage ne peut rien faire avant son prochain tour de jeu.
        Il peut emporter des objets si leur poids total ne dépasse pas sa charge maximale.
        Il peut aussi emmener une créature consentante de taille M ou inférieure (qui porte un équipement ou des objets qui ne dépassent pas sa charge maximale) ou son équivalent par tranche de trois niveaux de lanceur de sorts.
        Une créature de taille G compte comme deux créatures de taille M etc.
        Toutes les créatures transportées doivent être en contact les unes avec les autres et l'une d'elles doit toucher le personnage.
        Si le personnage et les créatures se matérialisent en un point déjà occupé par un solide, ils subissent 1d6 points de dégâts et sont expulsés vers une surface inoccupée située dans un rayon de trente mètres.
        S'il n'y a pas le moindre endroit propice dans un rayon de trente mètres, le personnage et les créatures qui l'accompagnant subissent 2d6 points de dégâts supplémentaires et sont expulsés vers un espace inoccupé situé dans un rayon de trois cents mètres.
        Si un tel endroit n'existe pas, ils subissent 4d6 points de dégâts de plus et le sort échoue.
        '''
    
    if name == 'Dissipation de la magie':
        description = '''
        Dissipation de la magie permet de mettre un terme à un sort lancé sur une créature ou un objet, d'annuler temporairement les pouvoirs d'un objet magique ou de contrer un sort jeté par un autre lanceur de sorts.
        Un sort dissipé se termine comme si sa durée normale était écoulée.
        Ce sort peut dissiper les effets des pouvoirs magiques de certaines créatures, mais pas les contrer au moment où ils sont lancés.
        Les effets des sorts dont la durée est instantanée ne peuvent pas être dissipés.
        Ce sort peut servir de deux façons différentes : ciblée ou contresort.
        Le personnage doit effectuer un test de dissipation (1d20 + son niveau de lanceur de sorts) et comparer le résultat avec le DD niveau de lanceur de sorts de l'effet ciblé (DD = 11 + niveau de lanceur de sorts).
        En cas de réussit il dissipe l'effet le plus puissant possible qui passe le DD.
        Le personnage peut viser un sort spécifique : .
        Un objet ou une créature qui est le produit d'un sort encore actif (comme par exemple un monstre appelé par convocation de monstres).
        Un objet magique, les propriétés magiques de l'objet sont désactivées pendant 1d4 rounds.
        Une interface extradimensionnelle (comme un sac sans fond), elle est temporairement fermée.
        Contresort : contre un sort en ciblant un jeteur de sorts, le personnage doit réussir un test de dissipation pour contrer le sort.
        '''
    
    if name == 'Fosse hérissée de pieux':
        description = '''
        Le personnage crée une fosse extradimensionnelle de 3m(2c) sur 3m(2c) et d'une profondeur de 3m(2c) par tranche de deux niveaux de lanceur de sorts (maximum 15 mètres (10 c)).
        Cette fosse doit être créée sur une surface horizontale d'une taille suffisante.
        Comme la fosse ne pèse rien et ne déplace pas le matériau qui se trouve en dessous.
        Toute créature qui se trouve dans la zone où il invoque la fosse doit réussir un jet de Réflexes pour ne pas tomber dedans.
        Les bords sont en pente, toute créature qui termine son tour sur une case adjacente à la fosse doit faire un jet de Réflexes avec un bonus de +2 pour ne pas glisser dedans.
        Les créatures soumises à un effet ayant pour but de les faire tomber dans la fosse (comme une bousculade) n'ont pas droit à un jet de sauvegarde pour éviter la chute.
        Le fond et les parois de la fosse sont tapissés de pointes acérées.
        Les créatures qui tombent dans la fosse reçoivent des dégâts de chute plus 2d6 points de dégâts perforants des pointes.
        Toute créature ou objet qui entre en contact avec celles des parois reçoit 1d6 points de dégâts perforants à chaque round passé en contact avec le mur.
        Le DD du test d'Escalade est de 20.
        Quand le sort se termine, les créatures qui se trouvent dans le trou remontent avec le fond de la fosse jusqu'à se retrouver à la surface, en un seul round.
        '''
    
    if name == 'Glace insidieuse':
        description = '''
        Ce sort couvre le sol, la terre ou toute autre surface horizontale calme (comme un lac tranquille ou une rivière placide) d'une pellicule de glace qui s'étend lentement.
        La pellicule de glace initiale ne peut pas se former dans une zone occupée par des objets ou des créatures physiques.
        Sa surface doit être lisse et sans rupture quand elle se crée.
        La glace est dure, solide et opaque, faisant 2,5 centimètres d'épaisseur par niveau de lanceur.
        La glace est suffisamment solide pour supporter le poids d'un cheval ordinaire, ce qui permet aux créatures de traverser une étendue d'eau.
        Chaque case de glace de 3m(2c) de côté a 3 points de vie par tranche de 2,5 centimètres d'épaisseur.
        Si une créature essaye de briser la glace sur une seule attaque, le DD du test de Force est égal à 15 + le niveau de lanceur du personnage.
        Chaque round, lors du tour du personnage, la pellicule de glace s'étend de 30 centimètres dans toutes les directions, sauf celle du personnage.
        Cette croissance est suffisamment lente pour que toutes les créatures présentes dans la zone aient le temps de la quitter ou de monter sur la glace.
        Si la glace qui s'étend recouvre complètement la case d'une créature, cette dernière doit décider si elle se place sur la pellicule de glace, si elle tombe sur la pellicule de glace ou si elle est repoussée dans une case adjacente.
        '''
    
    if name == 'Invisibilité suprême':
        description = '''
        La créature touchée devient invisible.
        Si le sort cible une créature qui transporte de l'équipement, celui-ci disparaît également.
        Si le lanceur du sort rend une autre créature invisible, ni lui ni ses alliés ne peuvent la voir.
        Les objets qu'une créature invisible lâche ou pose réapparaissent, alors que ceux qu'elle ramasse disparaissent si elle les camoufle dans ses vêtements ou dans un sac qu'elle transporte.
        Par contre, la lumière ne disparaît jamais, même si sa source peut devenir invisible (l'effet produit est donc celui d'une lumière sans source visible).
        Si une créature invisible porte un objet très grand, toute partie de celui-ci située à plus de 3m(2c) d'elle est visible.
        Une créature invisible n'est pas forcément silencieuse, et certaines situations peuvent la rendre facilement détectable (si elle nage ou marche dans une une flaque par exemple).
        Une créature invisible et immobile bénéficie d'un bonus de +40 aux tests de Discrétion.
        Ce bonus est réduit à +20 si la créature se déplace.
        Ce sort fonctionne comme invisibilité mais l'effet ne s'interrompt pas lorsque le sujet attaque un adversaire.
        '''
    
    if name == 'Localisation de créature':
        description = '''
        Grâce à cette incantation, le personnage sent dans quelle direction se trouve une créature connue du personnage.
        A la fin de l'incantation, le personnage fait lentement des tours sur lui-même, jusqu'à ce que le sort lui indique dans quelle direction se trouve la créature qu'il recherche (si elle ne se trouve pas trop loin).
        Si elle se déplace, le jeteur de sorts apprend également dans quelle direction elle avance.
        Le sort permet de localiser une créature d'une espèce spécifique autant que quelqu'un connu personnellement par le personnage.
        Il ne peut pas localiser une créature d'un type général.
        Le personnage doit avoir vu un spécimen de l'espèce recherchée au moins une fois dans sa vie à 9m(6c) min.
        Les cours d'eau bloquent le sort.
        Il ne permet pas de localiser les objets.
        Il peut être trompé par des sorts comme antidétection, double illusoire et métamorphose.
        '''
    
    if name == 'Morsure magique suprême':
        description = '''
        Morsure magique donne à l'une des armes naturelles de la cible un bonus d'altération de +1 tous les quatre niveaux du personnage (jusqu'à un maximum de +5) aux jets d'attaque et de dégâts. Ce bonus ne permet pas aux armes naturelles ou aux attaques à mains nues de franchir la réduction de dégâts, hormis pour la magie. Le sort peut affecter une morsure, un coup de poing, ou toute autre attaque portée par une arme naturelle. Ce sort ne transforme pas les dégâts non-létaux des attaques à mains nues en dégâts létaux.
        Une autre version du sort existe et confère un bonus d'altération de +1 à toutes les armes naturelles de la créature (quel que soit le niveau de lanceur de sorts du personnage).
        Il est possible d'user de permanence sur un sort de morsure magique suprême.
        '''
    
    if name == 'Mur de feu':
        description = '''
        Un rideau immobile de flammes violettes se constitue au terme de l'incantation.
        Un côté du mur, choisi par le personnage, dégage une chaleur infligeant 2d4 points de dégâts de feu à toutes les créatures situées à 3m(2c) ou moins des flammes, et 1d4 points à celles se trouvant entre 3 et 6 m (2 à 4 c) de distance.
        Le mur inflige ces dégâts lorsqu'il apparaît et à chaque round où un individu se trouve dans la zone d'effet du sort au tour du personnage.
        De plus, il inflige 2d6 points de dégâts de feu, +1 par niveau de lanceur de sorts (jusqu'à un maximum de +20) à tous les personnages qui le traversent.
        Ces dégâts sont doublés pour les morts-vivants.
        Si le personnage fait apparaître le mur à l'endroit où se trouvent une ou plusieurs créatures, ces dernières subissent des dégâts identiques à la traversée des flammes. Si une portion de mur de 1,50 m (1 case) de large subit au moins 20 points de dégâts de froid en 1 round, elle disparaît (on ne divise pas par quatre les dégâts infligés par le froid, comme cela est normalement le cas pour les objets).
        On peut user de permanence sur un sort de mur de feu. Un mur de feu permanent soufflé par des dégâts de froid est inactif pendant dix minutes, puis redevient parfaitement opérationnel.
        '''
    
    if name == 'Mur de glace':
        description='''
        Ce sort fait apparaître un mur ou un hémisphère de glace, au choix du lanceur de sort.
        On ne peut pas le faire apparaître à un endroit occupé (objets ou créatures) et sa surface doit être lisse et sans fissure.
        Toute créature adjacente au mur lors de sa création a droit à un jet de Réflexes pour le détruire tandis qu'il se forme.
        Le mur est vulnérable au feu qui lui inflige des dégâts normaux, si le mur fond le choc thermique libère un nuage de brouillard qui persiste pendant dix minutes.
        Plan de glace.
        La barrière prend la forme d'une couche de glace plane, de 2,5 cm d'épaisseur et couvre un carré de 3m(2c) de côté avec 3 PV par niveau de lanceur de sorts.
        Le plan peut être orienté dans n'importe quelle direction, du moment qu'il est ancré à un support.
        Tout personnage qui attaque le mur le touche automatiquement ; Pour percer la barrière d'un seul coup, le DD du test de Force est égal à 15 + niveau de lanceur de sorts; Un trou creusé dans le mur libère une vague de froid qui inflige 1d6 points de dégâts de froid, +1 par niveau de lanceur de sorts (sans jet de sauvegarde) à quiconque passe par l'ouverture ainsi créée.
        Hémisphère.
        Le sort prend la forme d'un hémisphère dont le rayon est égal ou inférieur à 90 cm + 30 cm par niveau de lanceur de sorts du mage ; Il est aussi résistant que le plan de glace décrit ci-dessus, mais n'inflige aucun dégât aux personnages qui passent par une brèche.
        '''
    
    if name == 'Protection du compagnon':
        description='''
        Ce sort crée un lien mystique spécial entre le personnage et son compagnon (compagnon animal, monture liée, eidolon ou familier), ce qui permet au premier de transférer les blessures du second à lui-même.
        La créature gagne un bonus de parade de +1 à la CA et un bonus de résistance de +1 aux jets de sauvegarde.
        Par une action immédiate, quand son compagnon reçoit des dégâts, le personnage peut recevoir ces dégâts à sa place pour empêcher son compagnon d'être blessé.
        (comme protection d'autrui, à la seule différence que les dégâts ne sont pas répartis entre le personnage et la cible).
        Les préjudices qui n'impliquent pas la perte de points de vie, comme les effets de charme, les affaiblissements temporaires de caractéristique, les absorptions de niveaux et les effets de morts, ne sont pas affectés.
        Si la créature souffre d'une réduction de points de vie à cause d'une valeur de Constitution réduite, le personnage ne peut pas recevoir ces dégâts à la place de son compagnon car ce ne sont pas des dégâts de points de vie.
        Quand le sort se termine, les dégâts que le personnage a subis grâce au sort ne sont pas réassignés à son compagnon.
        Si le personnage et son compagnon ne sont plus à portée l'un de l'autre, le sort se termine.
        '''
        
    if name == 'Possession de marionnette':
        description = description.replace('capacités', 'capa')
        
    if name.startswith('Cercle magique contre'):
        description = "Voir la page en plus"
    
    auto_format_description(contents, description)


def auto_format_description(contents, description):
    description = re.sub('Si le personnage dépense (.*) utilisations de pouvoir mythique,', r'Pour \1 PM :', description)
    
    description = base_replace_in_desc(description)
    
    for line in description.split('.'):
        line = line.strip()
        if line:
            if line == '{EMPTYLINE}':
                contents.append('text | ')
            elif line.startswith('{BULLET}'):
                contents.append('bullet | %s.' % line[len('{BULLET} '):])
            elif line.startswith('{SECTION}'):
                contents.append('section | %s.' % line[len('{SECTION} '):])
            else:
                if 'user de permanence' in line:
                    line = '<icon name="info" size="8" > %s' % line
                contents.append('text | %s.' % line)


def format_mythic_description(spell, contents):
    
    name = spell['name']
    description = spell['description_mythic']
    section = 'section | Mythique'
    
    # if name == 'Tentacules noirs':
    #     return
    # if name == 'Porte dimensionnelle':
    #     return
    
    description = description.split('.')
    lines = []
    for line in description:
        line = line.strip()
        if line.startswith('Amplifié'):
            section = 'section | Mythique - %s' % line
        else:
            lines.append(line)
    description = '.'.join(lines)
    
    # if name == 'Agrandissement':
    #     description = '''
    #     Catégorie de taille +2 cran, max : Très Grand.
    #     '''
    # if name == 'Rapetissement':
    #     description = '''
    #     Catégorie de taille -2 cran, max : Très Petit.
    #     '''
    # elif name == 'Endurance aux énergies destructives':
    #     description = '''
    #     La cible du sort change et devient une créature touchée par niveau.
    #     Les créatures affectées gagnent une résistance au froid et au feu de 5.
    #     Elles ne sont pas ralenties par la neige et ignorent les malus aux tests de Perception et aux attaques d'armes à distance infligés par la grêle, la pluie, la neige fondue ou normale.
    #     Elles considèrent que la puissance du vent est inférieure d'une catégorie.
    #     '''
    # elif name == 'Feuille morte':
    #     description = '''
    #     +1 cible par lvl du personnage, plus de limite de distance entre les cibles.
    #     Pour 2 utilisat° de PM : La vitesse de chute devient normale (sans dgm), à l'atterrisage -> rayonnement de force de 3m inflige 1d6 dgm par NLS (5d6 max, Réflexes ½ dgm, DD de feuille morte).
    #     Les cibles pas affectées par explosions..
    #     '''
    # elif name == 'Graisse':
    #     description = '''
    #     + grade au DD des tests d'Acrobaties et au bonus en cas de lutte.
    #     Amplifié : Pour 2 utilisat° de PM : graisse inflammable, si ds la zone en feu 1d3 dgm lors du tour du personnage, si créature graissée 2d6 dgm de feu, + grade au DD du jet de sauvegarde.
    #     '''
    # elif name.startswith('Protection contre'):
    #     '''
    #     Les bonus à la CA et aux jets de sauvegarde s'élèvent à +4. Les créatures Mauvaises qui tentent de posséder ou de contrôler mentalement la cible doivent effectuer un jet de Volonté contre ce sort. Si la créature rate son jet, elle subit 1d6 points de dégâts par tranche de 2 niveaux de lanceur de sorts (5d6 maximum) à cause du contrecoup mental.
    #     Une créature Mauvaise qui utilise la résistance à la magie pour ignorer la protection conférée par le sort contre tout contact doit réussir un jet de sauvegarde ou subir ces dégâts une fois par round tant qu'elle elle attaque la cible protégée.
    #     '''
    #     replaced = [
    #         ('''Les créatures .* qui tentent de posséder ou de contrôler mentalement la cible doivent effectuer un jet de Volonté contre ce sort.''', '''Si tentative de contrôle mental :  jet de Volonté contre ce sort, '''),
    #         ('''Si la créature rate son jet,''', '''si échec'''),
    #         ('''s'élèvent à''', '''passe à'''),
    #         (
    #             '''Une créature .* qui utilise la résistance à la magie pour ignorer la protection conférée par le sort contre tout contact doit réussir un jet de sauvegarde ou subir ces dégâts une fois par round tant qu'elle elle attaque la cible protégée.''',
    #             '''Si la créature convoquée passe la RM, elle doit faire un JdS sinon elle prend les dmg une fois par round.'''),
    #         ('''e personnage''', '''a cible'''),
    #         ('''jets de sauvegarde''', '''JdS'''),
    #         ('''jet de sauvegarde''', '''JdS'''),
    #         (r'''\+2 aux JdS.''', '''+2 aux JdS, '''),
    #         ('''contrôle mental''', '''ctrl mental'''),
    #         ('''contre ''', '''vs '''),
    #         ('''dégâts''', '''dgm'''),
    #         ('''niveaux de lanceur de sorts''', '''NLS'''),
    #         ('''niveaux''', '''lvl'''),
    #         ('''maximum''', '''max'''),
    #     ]
    #
    #     for r in replaced:
    #         description = re.sub(*r, description)
    #
    # if name == 'Coursier fantôme':
    #     description = '''
    #     Pour le personnage auto réussit des tests d'Équitation pour rester en selle, les points de vie du coursier s'élèvent à 10 + 2x niveau de lanceur de sorts et sa VD à 9m x ½ NLS (45m maximum).
    #     Pour 2 PM : le coursier est intangible, mais le personnage peut interagir normalement avec lui.
    #     '''
    #
    # if name == 'Nuée grouillante':
    #     description = '''
    #     La nuée est dotée de l'archétype simple de créature évoluée.
    #     Une nuée de rats ou d'araignées laisse derrière elle une traînée de déchets ou de pus sanguinolent lorsqu'elle se déplace, cette traînée produit les effets d'un sort de graisse pendant un round, les créatures qui entrent ou traversent cette traînée sont également victimes de la maladie ou du poison propagé par la nuée, comme si celle-ci les avait attaquées directement, une créature à terre dans la traînée subit un malus de -4 au jet de sauvegarde contre cette affliction.
    #     Une nuée de chauve-souris laisse une traînée similaire qui persiste pendant 2 rounds mais qui ne produit aucun effet de poison ou de maladie.
    #     '''
    # if name == 'Rapidité':
    #     description = '''
    #     + 1 action de mouvement chaque round, VD +15 (max x3 VD normal).
    #     Pour 2 PM : VD +21m sans limite, Si VD ≥ 9m, elle peut marcher sur une surface liquide, si le liquide inflige des dégâts, la créature subit ½ dégâts.
    #     '''
    # if name == 'Pattes d\'araignée':
    #     description = description.replace(''' (y compris les toiles d'araignée géante)''', '')
    #
    # if name == 'Lévitation':
    #     description = '''
    #     Nb max de cible : niveau de lanceur de sorts, poids total limité à 50 kilogrammes x niveau de lanceur de sorts, le mouvement déplace toutes les cibles de la même façon.
    #
    #     Pour 10 PM : fait léviter en permanence un cube de roche de 1,5m de côté sur 6mètres verticalement ou horizontalement par une action de mouvement.
    #     Les cubes supportent une charge de 500 kilogrammes et on peut agglomère les cubes pour construire un pont, un château flottant ou un édifice similaire.
    #     '''
    #
    # if name == 'Invisibilité':
    #     description = '''
    #     L'invisibilité ne peut être révélée par des sorts de niveau 2 ou moins, mais une vision lucide ou de la poudre d'apparition fonctionne.
    #     Pour 2 PM : la vision aveugle, la perception aveugle, l'odorat et la perception des vibrations ne suffisent pas à détecter.
    #     '''
    
    # if name == 'Vol':
    #     description = '''La vitesse de déplacement en vol s'élève à 36 mètres (ou 24 mètres) avec une manoeuvrabilité parfaite.
    #     Lorsque la durée du sort est écoulée, la cible est protégée par un sort de feuille morte pendant un nombre de rounds égal au niveau de lanceur de sorts du personnage.
    #     Si le personnage dépense deux utilisations de pouvoir mythique, la cible ajoute le grade du personnage à ses jets de Réflexes et en bonus d'esquive à sa CA, uniquement lorsqu'elle vole.'''
    
    contents.append('text | ')
    contents.append(section)
    
    auto_format_description(contents, description)


def format_school(spell):
    
    school = spell['school'].lower()
    
    for s, i in SCHOOL_ICON.items():
        school = school.replace(s, i)
    
    return school


def format_effect(spell):
    name = spell['name']
    effect = spell['effect']
    
    if name == 'Signature magique':
        effect = '1 rune devant tenir dans un carré de 30 cm de côté'
    if name == 'Bouclier de pierre':
        effect = 'mur de pierre de 1,5m de côté'
    if name == 'Convocation de monstres I':
        effect = '1 créature de niveau 1 convoquées'
    
    effect = effect.replace('niveaux', 'lvl')
    effect = effect.replace('niveau', 'lvl')
    effect = effect.replace('''plan de glace constitué d'un carré''', 'plan de glace carré')
    return effect


def get_title_size(spell):
    title_size = None
    name = spell['name']
    if name == 'Convocation de monstres mineurs':
        title_size = 10
    elif name.startswith('Convocation de monstres VIII'):
        title_size = 10
    elif name.startswith('Convocation de monstres'):
        title_size = 11
    elif 'partagé' in name and name.startswith('Protection contre'):
        title_size = 10
    elif name == 'Protection contre les projectiles':
        title_size = 10
    elif name == 'Endurance aux énergies destructives':
        title_size = 10
    elif name == 'Résistance aux énergies destructives':
        title_size = 10
    elif name.startswith('Protection contre'):
        title_size = 11
    elif name.startswith('Cercle magique contre'):
        title_size = 10
    elif name == 'Détection de l\'invisibilité':
        title_size = 12
    elif name == 'Agrandissement de groupe':
        title_size = 12
    elif name == 'Restauration d\'eidolon mineure':
        title_size = 10
    elif name == 'Régénération d\'eidolon mineure':
        title_size = 10
    elif name == 'Charge de fourmi (partagé)':
        title_size = 12
    elif name == 'Communication avec les animaux':
        title_size = 10
    elif name == 'Contrôle des créatures convoquées':
        title_size = 11
    elif name == 'Convocation de créature totémique':
        title_size = 10
    elif name == 'Coursier fantôme (partagé)':
        title_size = 12
    elif name == 'Résistance aux énergies destructives (partagé)':
        title_size = 10
    elif name == 'Thaumaturgie associative':
        title_size = 12
    elif name == 'Protection du compagnon':
        title_size = 12
    elif name == 'Protection contre les énergies destructives':
        title_size = 10
    elif name == 'Possession de marionnette':
        title_size = 12
    elif name == 'Morsure magique suprême':
        title_size = 10
    elif name == 'Mordre la main de son maître':
        title_size = 11
    elif name == '''Pattes d'araignée (partagé)''':
        title_size = 12
    elif name == '''Dissipation de la magie''':
        title_size = 11
    elif name == '''Porte dimensionnelle''':
        title_size = 12
    return title_size


def get_card_name(spell):
    name = spell['name']
    if name == 'Convocation de monstres mineurs':
        name = 'Convocation de monstres min'
    if name.startswith('Protection contre'):
        name = name.replace('contre', 'vs')
    if name == 'Endurance aux énergies destructives':
        name = 'Endu aux énergies destruct'
    if name == 'Résistance aux énergies destructives':
        name = 'Résist aux énergies destruct'
    if name == 'Protection contre les projectiles':
        name = 'Protect° contre les projectiles'
    if name == 'Contrôle des créatures convoquées':
        name = 'Contrôle des créa convoquées'
    if name == 'Convocation de créature totémique':
        name = 'Convocation de créa totémique'
    if name == 'Protection vs les projectiles (partagé)':
        name = 'Protect° vs les proj (partagé)'
    if name == 'Résistance aux énergies destructives (partagé)':
        name = 'Résist° vs NRJ destruc (partagé)'
    if name == 'Protection contre les énergies destructives':
        name = 'Protect° vs énergies destructives'
    
    if spell['description_mythic']:
        name = '⭐ - %s' % name
    return name


def format_pj_class(pj_class):
    pj_class = pj_class.lower()
    if pj_class == 'barde':
        pj_class = 'bard'
    if pj_class == 'alchimiste':
        pj_class = 'alc'
    if pj_class == 'conjurateur':
        pj_class = 'conj'
    if pj_class == 'sorcière':
        pj_class = 'sor'
    if pj_class == 'sanguin':
        pj_class = 'sang'
    if pj_class == 'antipaladin':
        pj_class = 'antip'
    if pj_class == 'prêtre':
        pj_class = 'prê'
    if pj_class == 'druide':
        pj_class = 'dru'
    
    if pj_class in ('ens/mag', 'ensorceleur/magicien'):
        return 'Ens/Mag'
    if pj_class == 'antip':
        return 'AntiP'
    else:
        return pj_class.capitalize()


def format_levels(spell, contents):
    spell_school = spell['school']
    contents.append('property | École : | %s' % format_school(spell))
    
    # levels = spell['level']
    # pj_class_by_level = {i: [] for i in range(10)}
    #
    # for entry in levels.split(','):
    #     k = entry.split()
    #     pj_class = k[0]
    #     level = k[1]
    #
    #     if pj_class.lower() in ('conu', 'conju'):
    #         continue
    #
    #     pj_class_by_level[int(level)].append(format_pj_class(pj_class))
    #
    # if '(' in spell_school or '[' in spell_school:
    #     contents.append('property | École : | %s' % format_school(spell))
    #     for i in range(10):
    #         z = pj_class_by_level[i]
    #         if z:
    #             contents.append(('property | LvL [%s] : | %s' % (i, '/'.join(z))).replace('/', ' / '))
    # else:
    #     first_done = False
    #     for i in range(10):
    #         z = pj_class_by_level[i]
    #         pj_class = '/'.join(z).replace('/', ' / ')
    #         if z:
    #             if first_done:
    #                 contents.append('property | LvL [%s] : | %s' % (i, pj_class))
    #             else:
    #                 contents.append('property | École : | %s <b>LvL [%s] : </b>%s' % (format_school(spell), i, pj_class))
    #                 first_done = True


def format_target(spell, contents):
    target = spell['target']
    if target:
        target = target.replace('500 g/niveau', '500 g/lvl')
        target = target.replace('50 kg/niveau', '50 kg/lvl')
        target = target.replace('(voir texte)', '')
        
        if target.startswith('ou zone d\'effet '):
            target = target[len('ou zone d\'effet '):]
            contents.append('property | Cible ou zone d\'effet :  | %s' % target)
        else:
            contents.append('property | Cible :  | %s' % target)
    
    return target


def build_card(spell, cards, icon='', color='', text_copyright=''):
    name = spell['name']
    level = get_level(spell, my_pj_class)
    contents = []
    
    format_levels(spell, contents)
    
    if spell['casting_time']:
        contents.append('property | Incantation :  | %s' % format_casting_time(spell))
    if spell['components']:
        contents.append('property | Compo :  | %s' % format_components(spell))
    if spell['range']:
        contents.append('property | Portée :  | %s' % format_range(spell))
    if spell['effect']:
        contents.append('property | Effet :  | %s' % format_effect(spell))
    if spell['area_effect']:
        contents.append('property | Zone d\'effet :  | %s' % format_area_effect(spell))
    
    format_target(spell, contents)
    
    if spell['duration']:
        contents.append('property | Durée :  | %s' % format_duration(spell))
    
    saving_throw_ = spell['saving_throw']
    saving_throw_ = saving_throw_.replace('(voir texte)', '')
    saving_throw_ = saving_throw_.replace('(voir description)', '')
    saving_throw_ = saving_throw_.replace('(cf. texte)', '')
    if saving_throw_ and spell['magic_resistance']:
        if '(' in spell['magic_resistance'] or '[' in spell['magic_resistance']:
            contents.append('property | JdS : | %s' % saving_throw_)
            contents.append('property | RM : | %s' % spell['magic_resistance'])
        else:
            if saving_throw_ == 'Volonté pour annuler (inoffensif)':
                saving_throw_ = 'Vol pr annuler (inoffensif)'
            if saving_throw_ == 'Volonté, annule (voir description)':
                saving_throw_ = 'Volonté, annule (voir des)'
            contents.append('property | JdS :  | %s <b>RM : </b> %s' % (saving_throw_, spell['magic_resistance']))
    else:
        if saving_throw_:
            contents.append('property | JdS : | %s' % saving_throw_)
        if spell['magic_resistance']:
            contents.append('property | RM : | %s' % spell['magic_resistance'])
    
    contents.append('rule')
    if spell['description']:
        format_description(spell, contents)
    # if spell['description_mythic']:
    #     format_mythic_description(spell, contents)
    if name not in ('Résistance aux énergies destructives', 'Vol'):
        if spell['info']:
            contents.append('text | <icon name="info" size="8" > %s' % spell['info'])
        if spell['speciality']:
            contents.append('text | <icon name="info" size="8" > %s' % spell['speciality'])
    
    if not text_copyright:
        text_copyright = 'Sorts %s - niveau %s - %s/%s' % (my_pj_class.name, level, spell['index'], spell['total_by_level'])
    copy_img = '<img src="img/Pathfinder-Logo-600x257.png" style=" position: relative;    top: -6px; width: 56px;    height: 24px;"> '
    card_copyright = copy_img + '<span style="text-align: right;width: 100%%;">%s</span>' % text_copyright
    card = build_card_dict(get_card_name(spell), contents, level, card_copyright, title_size=get_title_size(spell), icon=icon, color=color)
    
    if cards:
        cards[level].append(card)
    
    contents = []
    if spell['description_mythic']:
        format_mythic_description(spell, contents)
        card_mythic = build_card_dict(get_card_name(spell), contents, level, card_copyright, title_size=get_title_size(spell), icon=icon, color=color)
        
        if cards:
            cards[level].append(card_mythic)
        
        return [card, card_mythic]
    
    return [card]


def build_cards_help(cards):
    contents = [
        'subtitle | École',
        'rule ',
    ]
    for s, i in SCHOOL_ICON.items():
        contents.append('property | | %s : %s' % (i, s.capitalize()))
    
    copy_img = '<img src="img/Pathfinder-Logo-600x257.png" style=" position: relative;    top: -6px; width: 56px;    height: 24px;"> '
    card_copyright = copy_img + '<span style="text-align: right;width: 100%%;">Sorts %s</span>' % my_pj_class.name
    card = build_card_dict('Légende', contents, None, card_copyright)
    cards.append(card)
    
    _abs = [
        ('dés de vie', 'DV'),
        ('vitesse de déplacement', 'VD'),
        ('niveau de lanceur de sort', 'NLS'),
        ('point de vie', 'PV'),
        ('dégât', 'dgm'),
        ('niveau', 'lvl'),
        ('jet de sauvegarde', 'JdS'),
        ('jet d\'attaque', 'JdA'),
        ('bonus de base à l\'attaque', 'BBA'),
        ('action simple', 'AS'),
        ('corps à corps', 'CàC'),
        ('pouvoir mythique', 'PM'),
    ]
    _abs.sort()
    
    contents = [
        'subtitle | Abréviation',
        'rule ',
    ]
    for a, b in _abs:
        contents.append('description  | %s : |  %s' % (b, a))
    
    copy_img = '<img src="img/Pathfinder-Logo-600x257.png" style=" position: relative;    top: -6px; width: 56px;    height: 24px;"> '
    card_copyright = copy_img + '<span style="text-align: right;width: 100%%;">Sorts %s</span>' % my_pj_class.name
    card = build_card_dict('Légende', contents, None, card_copyright)
    cards.append(card)


def main():
    if not os.path.exists('out/spells-%s.csv' % my_pj_class.name):
        print('out/spells-%s.csv' % my_pj_class.name)
        return
    spells = read_spell_file()
    
    all_cards = []
    cards = {i: [] for i in range(10)}
    build_cards_help(cards[0])
    all_cards.extend(cards[0])
    
    for spell in spells:
        card = build_card(spell, cards)
        all_cards.extend(card)
        # print(card)
    
    for lvl, c in cards.items():
        if c:
            with open('out/cards-%s-lvl-%s.json' % (my_pj_class.name, lvl), 'w', encoding='utf-8') as fd:
                json.dump(c, fd)
    
    with open('out/cards-%s.json' % my_pj_class.name, 'w', encoding='utf-8') as fd:
        json.dump(all_cards, fd)


if __name__ == '__main__':
    main()
