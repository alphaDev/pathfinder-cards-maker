#!/usr/bin/python
# -*- coding: utf-8 -*-

import csv
import json
import math
import os
import re

from common import build_table, build_dndstats, base_replace_in_desc, TO_ROMAN
from summon_scraper import Monster, Mephit

WITH_ILLU = False

SUMMON_PATH = 'out/summon-lvl-%s.csv'

ICON_SIZE = 11

FEATS_FILTER = ('Science de l\'initiative', 'Discrétion)', 'Réflexes surhumains', 'Robustesse', 'Volonté de fer', 'Vigilance', 'Pouvoir magique rapide (invisibilité)', 'Arme de prédilection (chaîne)')

SUMMON_INFO = {
    "Aigle"                               : {'extra_card': False, 'icon': 'eagle-head', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/hawk.png', 'in_extra': True}},
    "Chien"                               : {'extra_card': False, 'icon': 'sitting-dog', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/dog.png', 'in_extra': True}},
    "Dauphin"                             : {'extra_card': False, 'icon': 'dolphin', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/dolphin.png', 'in_extra': True}},
    "Grenouille venimeuse"                : {'extra_card': False, 'icon': 'frog', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/frog.png', 'in_extra': True}},
    "Poney"                               : {'extra_card': False, 'icon': 'horse-head-2', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/donkey.png', 'in_extra': True}},
    "Punaise de feu"                      : {'extra_card': False, 'icon': 'spotted-bug', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/fire bug.png', 'in_extra': True}},
    "Rat sanguinaire"                     : {'extra_card': False, 'icon': 'rat', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/rat.png', 'in_extra': True}},
    "Vipère"                              : {'extra_card': False, 'icon': 'snake', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/snake 01.png', 'in_extra': True}},
    "Araignée géante"                     : {'extra_card': False, 'icon': 'spider-alt', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/spider.png', 'in_extra': True}},
    "Calmar"                              : {'extra_card': False, 'icon': 'giant-squid', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/dark mantle.png', 'in_extra': True}},
    "Cheval"                              : {'extra_card': False, 'icon': 'horse-head-2', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/horse.png', 'in_extra': True}},
    "Chien gobelin"                       : {'extra_card': False, 'icon': 'sitting-dog', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/goblien dog.png', 'in_extra': True}},
    "Fourmi géante, ouvrière"             : {'extra_card': False, 'icon': 'ant', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/ant.png', 'in_extra': True}},
    "Grenouille géante"                   : {'extra_card': False, 'icon': 'frog', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/toad.png', 'in_extra': True}},
    "Hyène"                               : {'extra_card': False, 'icon': 'sharp-smile', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/hyenna.png', 'in_extra': True}},
    "Lémure"                              : {'extra_card': False, 'icon': 'slime', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/lemure.png', 'in_extra': True}},
    "Loup"                                : {'extra_card': False, 'icon': 'wolf-head', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/wolf.png', 'in_extra': True}},
    "Mille-pattes géant"                  : {'extra_card': False, 'icon': 'centipede', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/centipede.png', 'in_extra': True}},
    "Pieuvre"                             : {'extra_card': False, 'icon': 'octopus', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/octopus 01.png', 'in_extra': True}},
    "Élémentaire de la Terre de taille P" : {'extra_card': False, 'icon': 'earth-spit', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/elemental earth.png', 'in_extra': True}},
    "Élémentaire de l'Air de taille P"    : {'extra_card': False, 'icon': 'whirlwind', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/elemental air.png', 'in_extra': True}},
    "Élémentaire de l'Eau de taille P"    : {'extra_card': False, 'icon': 'water-splash', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/elemental water.png', 'in_extra': True}},
    "Élémentaire du Feu de taille P"      : {'extra_card': False, 'icon': 'fire-zone', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/elemental fire.png', 'in_extra': True}},
    "Anguille électrique"                 : {'extra_card': False, 'icon': 'eel', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/eel.png', 'in_extra': True}},
    "Archon lumineux"                     : {'extra_card': False, 'icon': 'fairy', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/lantern archon.png', 'in_extra': True}},
    "Auroch"                              : {'extra_card': False, 'icon': 'charging-bull', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/aurochs.png', 'in_extra': True}},
    "Chauve-souris sanguinaire"           : {'extra_card': False, 'icon': 'swamp-bat', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/bat.png', 'in_extra': True}},
    "Crocodile"                           : {'extra_card': False, 'icon': 'croc-jaws', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/crocodile 01.png', 'in_extra': True}},
    "Dretch"                              : {'extra_card': False, 'icon': 'sharped-teeth-skull', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/dretch.png', 'in_extra': True}},
    "Fourmi géante, soldat"               : {'extra_card': False, 'icon': 'ant', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/big ant 02.png', 'in_extra': True}},
    "Glouton"                             : {'extra_card': False, 'icon': 'polar-bear', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/badger.png', 'in_extra': True}},
    "Gorille"                             : {'extra_card': False, 'icon': 'gorilla', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/gorilla 01.png', 'in_extra': True}},
    "Guépard"                             : {'extra_card': False, 'icon': 'saber-toothed-cat-head', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/cheetah.png', 'in_extra': True}},
    "Léopard"                             : {'extra_card': False, 'icon': 'top-paw', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/leopard.png', 'in_extra': True}},
    "Requin"                              : {'extra_card': False, 'icon': 'shark-jaws', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/shark 01.png', 'in_extra': True}},
    "Sanglier"                            : {'extra_card': False, 'icon': 'boar', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/boar.png', 'in_extra': True}},
    "Serpent constricteur"                : {'extra_card': False, 'icon': 'snake-tongue', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/snake 02.png', 'in_extra': True}},
    "Varan"                               : {'extra_card': False, 'icon': 'gecko', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/drake.png', 'in_extra': True}},
    "Archon canin"                        : {'extra_card': False, 'icon': 'labrador-head', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/hound archon.png', 'in_extra': True}},
    "Bison"                               : {'extra_card': False, 'icon': 'bull-horns', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/bison.png', 'in_extra': True}},
    "Déinonychus"                         : {'extra_card': False, 'icon': 'velociraptor', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/deinonychus.png', 'in_extra': True}},
    "Fourmi géante, mâle"                 : {'extra_card': False, 'icon': 'ant', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/big ant 01.png', 'in_extra': True}},
    "Gorille sanguinaire"                 : {'extra_card': False, 'icon': 'gorilla', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/gorilla 02.png', 'in_extra': True}},
    "Grizzly"                             : {'extra_card': False, 'icon': 'bear-head', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/bear 01.png', 'in_extra': True}},
    "Guêpe géante"                        : {'extra_card': False, 'icon': 'wasp-sting', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/wasp.png', 'in_extra': True}},
    "Lion"                                : {'extra_card': False, 'icon': 'lion', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/lion 01.png', 'in_extra': True}},
    "Loup sanguinaire"                    : {'extra_card': False, 'icon': 'wolf-head', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/dire wolf.png', 'in_extra': True}},
    "Molosse infernal"                    : {'extra_card': False, 'icon': 'hound', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/hell hound.png', 'in_extra': True}},
    "Ptéranodon"                          : {'extra_card': False, 'icon': 'pterodactylus', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/pteranodon.png', 'in_extra': True}},
    "Rhinocéros"                          : {'extra_card': False, 'icon': 'rhinoceros-horn', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/rhinoceros.png', 'in_extra': True}},
    "Sanglier sanguinaire"                : {'extra_card': False, 'icon': 'boar', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/boar 02.png', 'in_extra': True}},
    "Scorpion géant"                      : {'extra_card': False, 'icon': 'scorpion', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/giant scorpion.png', 'in_extra': True}},
    "Élémentaire de la Terre de taille M" : {'extra_card': False, 'icon': 'earth-spit', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/elemental earth.png', 'in_extra': True}},
    "Élémentaire de l'Air de taille M"    : {'extra_card': False, 'icon': 'whirlwind', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/elemental air.png', 'in_extra': True}},
    "Élémentaire de l'Eau de taille M"    : {'extra_card': False, 'icon': 'water-splash', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/elemental water.png', 'in_extra': True}},
    "Élémentaire du Feu de taille M"      : {'extra_card': False, 'icon': 'fire-zone', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/elemental fire.png', 'in_extra': True}},
    "Méphite"                             : {'extra_card': False, 'icon': 'imp', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/mephit.png', 'in_extra': True}},
    "Ankylosaure"                         : {'extra_card': False, 'icon': 'stegosaurus-scales', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/ankylosaurus.png', 'in_extra': True}},
    "Babau"                               : {'extra_card': False, 'icon': 'lizardman', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/babau.png', 'in_extra': True}},
    "Bralani"                             : {'extra_card': True, 'icon': 'arrow-wings', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/bralani.png', 'in_extra': True}},
    "Diable barbu"                        : {'extra_card': True, 'icon': 'beard', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/bearded.png', 'in_extra': False}},
    "Kyton"                               : {'extra_card': True, 'icon': 'crossed-chains', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/kyton.png', 'in_extra': True}},
    "Lion sanguinaire"                    : {'extra_card': False, 'icon': 'lion', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/lion 02.png', 'in_extra': True}},
    "Murène géante"                       : {'extra_card': False, 'icon': 'eel', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/eel.png', 'in_extra': True}},
    "Orque épaulard"                      : {'extra_card': False, 'icon': 'whale-tail', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/orca.png', 'in_extra': True}},
    "Rhinocéros laineux"                  : {'extra_card': False, 'icon': 'rhinoceros-horn', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/woolly rhinoceros.png', 'in_extra': True}},
    "Salamandre"                          : {'extra_card': False, 'icon': 'gecko', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/salamander.png', 'in_extra': True}},
    "Xill"                                : {'extra_card': True, 'icon': 'evil-minion', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/xill.png', 'in_extra': False}},
    "Élémentaire de la Terre de taille G" : {'extra_card': False, 'icon': 'earth-spit', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/elemental earth.png', 'in_extra': True}},
    "Élémentaire de l'Air de taille G"    : {'extra_card': False, 'icon': 'whirlwind', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/elemental air.png', 'in_extra': True}},
    "Élémentaire de l'Eau de taille G"    : {'extra_card': False, 'icon': 'water-splash', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/elemental water.png', 'in_extra': True}},
    "Élémentaire du Feu de taille G"      : {'extra_card': False, 'icon': 'fire-zone', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/elemental fire.png', 'in_extra': True}},
    "Démon des ombres"                    : {'extra_card': True, 'icon': 'shadow-grasp', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/shadow demon.png', 'in_extra': False}},
    "Élasmosaure"                         : {'extra_card': False, 'icon': 'sea-creature', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/elasmosaurus.png', 'in_extra': True}},
    "Éléphant"                            : {'extra_card': False, 'icon': 'elephant', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/elephant.png', 'in_extra': True}},
    "Érinye"                              : {'extra_card': True, 'icon': 'wing-cloak', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/erinyes.png', 'in_extra': True}},
    "Lillende"                            : {'extra_card': True, 'icon': 'harpy', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/lillend.png', 'in_extra': True}},
    "Ours sanguinaire"                    : {'extra_card': False, 'icon': 'bear-head', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/bear 02.png', 'in_extra': True}},
    "Pieuvre géante"                      : {'extra_card': False, 'icon': 'octopus', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/octopus 02.png', 'in_extra': True}},
    "Succube"                             : {'extra_card': True, 'icon': 'whip', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/succubus.png', 'in_extra': False}},
    "Tigre sanguinaire"                   : {'extra_card': False, 'icon': 'tiger-head', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/tiger.png', 'in_extra': True}},
    "Traqueur invisible"                  : {'extra_card': True, 'icon': 'invisible', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/ghost.png', 'in_extra': True}},
    "Tricératops"                         : {'extra_card': False, 'icon': 'triceratops-head', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/triceratops.png', 'in_extra': True}},
    "Élémentaire de la Terre de taille TG": {'extra_card': False, 'icon': 'earth-spit', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/elemental earth.png', 'in_extra': True}},
    "Élémentaire de l'Air de taille TG"   : {'extra_card': False, 'icon': 'whirlwind', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/elemental air.png', 'in_extra': True}},
    "Élémentaire de l'Eau de taille TG"   : {'extra_card': False, 'icon': 'water-splash', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/elemental water.png', 'in_extra': True}},
    "Élémentaire du Feu de taille TG"     : {'extra_card': False, 'icon': 'fire-zone', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/elemental fire.png', 'in_extra': True}},
    "Bébilith"                            : {'extra_card': True, 'icon': 'masked-spider', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/bebelith.png', 'in_extra': False}},
    "Brachiosaure"                        : {'extra_card': False, 'icon': 'diplodocus', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/diplodocus.png', 'in_extra': True}},
    "Calmar géant"                        : {'extra_card': False, 'icon': 'giant-squid', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/dark mantle.png', 'in_extra': True}},
    "Crocodile sanguinaire"               : {'extra_card': False, 'icon': 'croc-jaws', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/crocodile 02.png', 'in_extra': True}},
    "Diable osseux"                       : {'extra_card': False, 'icon': 'daemon-skull', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/bone devil.png', 'in_extra': True}},
    "Mastodonte"                          : {'extra_card': False, 'icon': 'mammoth', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/mammoth.png', 'in_extra': True}},
    "Requin sanguinaire"                  : {'extra_card': False, 'icon': 'shark-jaws', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/shark 02.png', 'in_extra': True}},
    "Roc"                                 : {'extra_card': False, 'icon': 'eagle-head', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/eagle.png', 'in_extra': True}},
    "Tyrannosaure"                        : {'extra_card': False, 'icon': 'dinosaur-rex', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/tyrannosaure.png', 'in_extra': True}},
    "Vrock"                               : {'extra_card': True, 'icon': 'spiky-wing', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/vrock.png', 'in_extra': False}},
    "Élémentaire de la Terre noble"       : {'extra_card': False, 'icon': 'earth-spit', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/elemental earth.png', 'in_extra': True}},
    "Élémentaire de l'Air noble"          : {'extra_card': False, 'icon': 'whirlwind', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/elemental air.png', 'in_extra': True}},
    "Élémentaire de l'Eau noble"          : {'extra_card': False, 'icon': 'water-splash', 'image': {'need_end_card': False, 'is_display': False, 'url': 'img/summon/elemental water.png', 'in_extra': True}},
    "Élémentaire du Feu noble"            : {'extra_card': False, 'icon': 'fire-zone', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/elemental fire.png', 'in_extra': True}},
    "Diable barbelé"                      : {'extra_card': True, 'icon': 'troglodyte', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/barbed devil.png', 'in_extra': True}},
    "Hezrou"                              : {'extra_card': True, 'icon': 'toad-teeth', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/hezrou.png', 'in_extra': True}},
    "Seigneur élémentaire de la Terre"    : {'extra_card': False, 'icon': 'earth-spit', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/elemental earth.png', 'in_extra': True}},
    "Seigneur élémentaire de l'Air"       : {'extra_card': False, 'icon': 'whirlwind', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/elemental air.png', 'in_extra': True}},
    "Seigneur élémentaire de l'Eau"       : {'extra_card': False, 'icon': 'water-splash', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/elemental water.png', 'in_extra': True}},
    "Seigneur élémentaire du Feu"         : {'extra_card': False, 'icon': 'fire-zone', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/elemental fire.png', 'in_extra': True}},
    "Archon messager"                     : {'extra_card': True, 'icon': 'bugle-call', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/trumpet archon.png', 'in_extra': False}},
    "Déva astral"                         : {'extra_card': True, 'icon': 'angel-outfit', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/astral deva.png', 'in_extra': True}},
    "Diable des glaces"                   : {'extra_card': True, 'icon': 'alien-bug', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/ice devil.png', 'in_extra': True}},
    "Ghaéle"                              : {'extra_card': True, 'icon': 'winged-sword', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/ghaele azata.png', 'in_extra': True}},
    "Glabrezu"                            : {'extra_card': True, 'icon': 'crab-claw', 'image': {'need_end_card': True, 'is_display': True, 'url': 'img/summon/glabrezu.png', 'in_extra': True}},
    "Nalfeshnie"                          : {'extra_card': True, 'icon': 'boar-tusks', 'image': {'need_end_card': False, 'is_display': True, 'url': 'img/summon/nalfeshnee.png', 'in_extra': True}},
    
}

MEPHIT_INFO = {
    "Méphite de l'Air"       : {'icon': 'whirlwind'},
    "Méphite de l'Eau"       : {'icon': 'water-splash'},
    "Méphite du Feu"         : {'icon': 'fire-bowl'},
    "Méphite de Glace"       : {'icon': 'snowflake-2'},
    "Méphite du magma"       : {'icon': 'volcano'},
    "Méphite de la poussière": {'icon': 'gold-nuggets'},
    "Méphite du sel"         : {'icon': 'sparkles'},
    "Méphite de la Terre"    : {'icon': 'earth-spit'},
    "Méphite de la vapeur"   : {'icon': 'steam'},
    "Méphite de la vase"     : {'icon': 'blood'},
}

MOD_SIZE = {
    'C'  : -8,
    'Gig': -4,
    'TG' : -2,
    'G'  : -1,
    'M'  : 0,
    'P'  : 1,
    'TP' : 2,
    'Min': 4,
    'I'  : 8,
}

FLYING_RANK = {
    'parfaite'  : '<b>A</b>',
    'parfait'   : '<b>A</b>',
    'bonne'     : '<b>B</b>',
    'moyenne'   : '<b>C</b>',
    'médiocre'  : '<b>D</b>',
    'déplorable': '<b>E</b>',
}

ENERGIES_TYPE = {
    'feu'           : '<span style="color:Red;"><icon name="flamer" size="%s" ></span>' % ICON_SIZE,
    'froid'         : '<span style="color:DeepSkyBlue;"><icon name="snowflake-2" size="%s" ></span>' % ICON_SIZE,
    'électricité'   : '<span style="color:Gold;"><icon name="electric" size="%s" ></span>' % ICON_SIZE,
    'acide'         : '<span style="color:Lime;"><icon name="acid" size="%s" ></span>' % ICON_SIZE,
    'poison'        : '<span style="color:Green;"><icon name="poison-bottle" size="%s" ></span>' % ICON_SIZE,
    'effets mentaux': '<span style="color:#d78692;"><icon name="brain" size="%s" ></span>' % ICON_SIZE,
    'maladie'       : '<span style="color:darkgoldenrod;"><icon name="vomiting" size="%s" ></span>' % ICON_SIZE,
    'pétrification' : '<span style="color:saddlebrown;"><icon name="stone-sphere" size="%s" ></span>' % ICON_SIZE,
}

SENSES = {
    'vision nocturne'             : '<icon name="night-sky" size="%s" >' % ICON_SIZE,
    'odorat'                      : '<icon name="sniffing-dog" size="%s" >' % ICON_SIZE,
    'vision aveugle'              : '<icon name="radar-sweep" size="%s" >' % ICON_SIZE,
    'perception des vibrations'   : '<icon name="vibrating-ball" size="%s" >' % ICON_SIZE,
    'vision dans le noir'         : '<icon name="night-vision" size="%s" >' % ICON_SIZE,
    'perception aveugle'          : '<icon name="sonic-screech" size="%s" >' % ICON_SIZE,
    'détection du Mal'            : '<icon name="aerial-signal" size="%s" > du Mal' % ICON_SIZE,
    'détection de l\'invisibilité': '<icon name="aerial-signal" size="%s" > Invisibilité' % ICON_SIZE,
    'vision dans les ténèbres'    : '<icon name="eye-target" size="%s" > ténèbres' % ICON_SIZE,
}

ACTION = {
    'lutte'        : '<icon name="push" size="%s" >' % ICON_SIZE,
    'croc-en-jambe': '<icon name="tripwire" size="%s" >' % ICON_SIZE,
    'étreinte'     : '<icon name="spiked-wall" size="%s" >' % ICON_SIZE,
}

SKILL = {
    'Acrobaties'                    : '<icon name="acrobatic" size="%s" >' % ICON_SIZE,
    'Art de la magie'               : '<icon name="pencil-brush" size="%s" >' % ICON_SIZE,
    'Artisanat'                     : '<icon name="anvil" size="%s" >' % ICON_SIZE,
    'Bluff'                         : '<icon name="public-speaker" size="%s" >' % ICON_SIZE,
    'Connaissances'                 : '<icon name="white-book" size="%s" >' % ICON_SIZE,
    'Déguisement'                   : '<icon name="prank-glasses" size="%s" >' % ICON_SIZE,
    'Diplomatie'                    : '<icon name="shaking-hands" size="%s" >' % ICON_SIZE,
    'Discrétion'                    : '<icon name="hooded-figure" size="%s" >' % ICON_SIZE,
    'Dressage'                      : '<icon name="whip" size="%s" >' % ICON_SIZE,
    'Équitation'                    : '<icon name="horse-head" size="%s" >' % ICON_SIZE,
    'Escalade'                      : '<icon name="mountain-climbing" size="%s" >' % ICON_SIZE,
    'Escamotage'                    : '<icon name="snatch" size="%s" >' % ICON_SIZE,
    'Estimation'                    : '<icon name="magnifying-glass" size="%s" >' % ICON_SIZE,
    'Évasion'                       : '<icon name="handcuffs" size="%s" >' % ICON_SIZE,
    'Intimidation'                  : '<icon name="muscle-up" size="%s" >' % ICON_SIZE,
    'Linguistique'                  : '<icon name="talk" size="%s" >' % ICON_SIZE,
    'Natation'                      : '<icon name="shark-fin" size="%s" >' % ICON_SIZE,
    'Perception'                    : '<icon name="eye-target" size="%s" >' % ICON_SIZE,
    'Premiers secours'              : '<icon name="health-normal" size="%s" >' % ICON_SIZE,
    'Profession'                    : '<icon name="freemasonry" size="%s" >' % ICON_SIZE,
    'Psychologie'                   : '<icon name="hive-mind" size="%s" >' % ICON_SIZE,
    'Représentation'                : '<icon name="drama-masks" size="%s" >' % ICON_SIZE,
    'Sabotage'                      : '<icon name="padlock-open" size="%s" >' % ICON_SIZE,
    'Survie'                        : '<icon name="camping-tent" size="%s" >' % ICON_SIZE,
    'Utilisation d\'objets magiques': '<icon name="crystal-wand" size="%s" >' % ICON_SIZE,
    'UOM'                           : '<icon name="crystal-wand" size="%s" >' % ICON_SIZE,
    'Vol'                           : '<icon name="liberty-wing" size="%s" >' % ICON_SIZE,
}


def build_cards_help():
    cards = []
    
    name = 'Légende'
    level = None
    icon = 'open-book'
    card_copyright = build_copyright()
    title_size = None
    
    contents = [
        'subtitle | Rang de Vol',
        'rule ',
    ]
    for s, i in {v: k for k, v in FLYING_RANK.items()}.items():
        contents.append('property | %s : | %s' % (s, i.capitalize()))
    
    contents.append('text |')
    contents.append('subtitle | Type d\'énergies')
    contents.append('rule ')
    for s, i in {v: k for k, v in ENERGIES_TYPE.items()}.items():
        contents.append('property | %s : | %s' % (s, i.capitalize()))
    
    card = build_card_dict(name, contents, level, card_copyright, icon, title_size)
    cards.append(card)
    
    name = 'Légende'
    level = None
    icon = 'open-book'
    card_copyright = build_copyright()
    title_size = None
    
    contents = [
        'subtitle | Sens',
        'rule '
    ]
    for s, i in {v: k for k, v in SENSES.items()}.items():
        contents.append('property | %s : | %s' % (s, i.capitalize()))
    
    contents.append('text |')
    contents.append('subtitle | Actions')
    contents.append('rule ')
    for s, i in {v: k for k, v in ACTION.items()}.items():
        contents.append('property | %s : | %s' % (s, i.capitalize()))
    
    card = build_card_dict(name, contents, level, card_copyright, icon, title_size)
    cards.append(card)
    
    name = 'Légende'
    level = None
    icon = 'open-book'
    card_copyright = build_copyright()
    title_size = None
    
    contents = []
    skill_as_table = []
    
    j = 0
    for s, i in {v: k for k, v in SKILL.items()}.items():
        if j % 2 == 0:
            skill_as_table.append([s, i.capitalize()])
        else:
            skill_as_table[-1].append(s)
            skill_as_table[-1].append(i.capitalize())
        j += 1
    
    contents.append('text | %s' % build_table(skill_as_table))
    
    card = build_card_dict(name, contents, level, card_copyright, icon, title_size)
    cards.append(card)
    
    name = 'Légende'
    level = None
    icon = 'open-book'
    card_copyright = build_copyright()
    title_size = None
    
    _abs = [
        ('dés de vie', 'DV'),
        ('vitesse de déplacement', 'VD'),
        ('niveau de lanceur de sort', 'NLS'),
        ('point de vie', 'PV'),
        ('dégât', 'dgm'),
        ('niveau', 'lvl'),
        ('jet de sauvegarde', 'JdS'),
        ('jet d\'attaque', 'JdA'),
        ('bonus de base à l\'attaque', 'BBA'),
        ('action simple', 'AS'),
        ('corps à corps', 'CàC'),
        ('pouvoir mythique', 'PM'),
    ]
    _abs.sort()
    contents = [
        'subtitle  | Abréviation ',
        'rule ',
    ]
    for a, b in _abs:
        contents.append('description  | %s : |  %s' % (b, a))
    
    card = build_card_dict(name, contents, level, card_copyright, icon, title_size)
    cards.append(card)
    
    name = 'Traits des élémentaires'
    level = None
    icon = 'dwennimmen'
    card_copyright = build_copyright()
    title_size = None
    
    contents = [
        "subtitle | Immunité",
        "rule",
        "text | la paralysie, le poison, les saignements, les effets de sommeil et l’étourdissement, coups critiques, tenaille, attaques de précision (comme les attaques sournoises)",
        "subtitle | Equipement",
        "rule",
        "property | Armes | Armes naturelles sauf armes citées dans leur profil.",
        "property | Armures | Cité dans leur profil. Les élémentaires qui sont formés au port d’au moins un type d’armures le sont également à l’utilisation des boucliers.",
        "rule",
        "text | Les élémentaires ne respirent pas, ne mangent pas et ne dorment pas."
    ]
    
    card = build_card_dict(name, contents, level, card_copyright, icon, title_size)
    cards.append(card)
    
    name = 'Capacités Spéciales'
    level = None
    icon = 'slap'
    card_copyright = build_copyright()
    title_size = None
    
    contents = [
        "text | ",
        "subtitle | Châtiment du Mal",
        "rule",
        "text | En une <b>action rapide</b>, contre une créature d'alignement <b>Mauvais</b>, ajouter le <b>bonus de Charisme</b> aux <b>jets d'attaque</b> et un <b>bonus aux dégâts</b> égal au nombre de  <b>DV</b>",
        "text | Persiste jusqu'à ce que la cible meure ou que la créature se repose",
        "text | ",
        "subtitle | Châtiment du Bien",
        "rule",
        "text | En une <b>action rapide</b>, contre une créature d'alignement <b>Bon</b>, ajouter le <b>bonus de Charisme</b> aux <b>jets d'attaque</b> et un <b>bonus aux dégâts</b> égal au nombre de  <b>DV</b>",
        "text | Persiste jusqu'à ce que la cible meure ou que la créature se repose"
    ]
    
    card = build_card_dict(name, contents, level, card_copyright, icon, title_size)
    cards.append(card)
    
    return cards


def read_summon_file(summon_level):
    summons = []
    with open(SUMMON_PATH % summon_level, 'r', encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            m = Monster()
            for k, v in row.items():
                if v is not None and v != '':
                    setattr(m, k, v)
            summons.append(m)
    
    return summons


def read_mephit_file():
    mephits = []
    with open('out/mephit.csv', 'r', encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            m = Mephit()
            for k, v in row.items():
                if v is not None and v != '':
                    setattr(m, k, v)
            mephits.append(m)
    
    return mephits


def build_card_dict(name, contents, level, card_copyright, icon, title_size):
    template = {
        'count'    : 1,
        'color'    : 'royalblue',
        'tags'     : ['lvl-%s' % level],
        'icon'     : icon,
        'title'    : name,
        'copyright': card_copyright,
        'contents' : contents,
    }
    if title_size is not None:
        template['title_size'] = title_size
    return template


def format_vd(summon: Monster):
    mov_list = summon.vd.split(',')
    vd = []
    
    while mov_list:
        mov = mov_list.pop(0)
        
        if '; sprint' in mov:
            mov = mov.replace('; sprint', '<b>sprint</b>')
        
        if mov.strip() == 'nage dans la terre':
            vd.append('<b>%s</b>' % mov)
        elif ' ' in mov:
            for r_name, r_logo in FLYING_RANK.items():
                mov = mov.replace(r_name, r_logo)
            mov_type, move_dist = mov.split(None, 1)
            if move_dist == '<b>sprint</b>':
                vd.append(mov_type + ' ' + move_dist)
                continue
            mov_type = mov_type.replace('propulsion', 'prop')
            mov_type = mov_type.replace('escalade', 'escal')
            vd.append('<b>%s</b>: %s' % (mov_type.capitalize(), move_dist))
        else:
            vd.append(mov)
    
    return ' '.join(vd)


def format_attack(contents, raw_text, prop_name):
    if not raw_text:
        return
    raw = raw_text.replace(',', '').split(')')
    for attack_info in raw:
        attack_info = attack_info.strip()
        if attack_info:
            attack_info = re.sub(r'(.*) [-+].* \((.*)', r'\1: <b>\2</b>', attack_info)
            if attack_info.startswith('et '):
                attack_info = attack_info[3:]
            attack_info = attack_info.strip()
            attack_info = attack_info.replace('deux', '2')
            attack_info = format_actions(attack_info)
            attack_info = format_energies_type(attack_info)
            if 'Cac' == prop_name:
                attack_info = attack_info + ' <b>[+2]</b>'
            if attack_info.startswith('ou '):
                contents[-1] = '%s %s' % (contents[-1], attack_info)
            else:
                contents.append('property | %s: |%s' % (prop_name, attack_info))


def format_energies_type(text):
    for k, v in ENERGIES_TYPE.items():
        text = re.sub(r'\b%s\b' % k, v, text)
    return text


def format_actions(text):
    for k, v in ACTION.items():
        text = re.sub(r'\b%s\b' % k, v, text)
    return text


def format_skill(text):
    text = text.lower()
    text = text.replace(' +', '&nbsp;+')
    for k, v in SKILL.items():
        text = text.replace(k.lower(), v)
    return text


def format_jds(contents, summon: Monster):
    if summon.saving_throws:
        saving_throws = summon.saving_throws
        saving_throws = saving_throws.replace('contre le', 'vs')
        saving_throws = saving_throws.replace('(résistance)', '')
        saving_throws = format_energies_type(saving_throws)
        contents.append('property | JdS:|%s' % saving_throws)


def compute_stats_and_mod(summon: Monster):
    raw_stats = summon.stats
    raw_stats = raw_stats.replace(',', '')
    _, for_val, _, dex_val, _, con_val, _, int_val, _, sag_val, _, cha_val = raw_stats.split()
    
    stats_and_mod = []
    for stat in (for_val, dex_val, con_val, int_val, sag_val, cha_val):
        if stat == '-':
            stats_and_mod.append(('-', '-'))
        else:
            stat = int(stat)
            mod = math.floor(((stat - 10) / 2))
            stats_and_mod.append((stat, mod))
    return stats_and_mod


def format_stats(contents, summon: Monster):
    raw_stats = summon.stats
    raw_stats = raw_stats.replace(',', '')
    _, for_val, _, dex_val, _, con_val, _, int_val, _, sag_val, _, cha_val = raw_stats.split()
    contents.append('text|%s' % build_dndstats((for_val, dex_val, con_val, int_val, sag_val, cha_val), True))


def format_magic_powers(contents, magic_powers, nls, title):
    if not magic_powers:
        return
    # contents.append('fill')
    contents.append('section | %s | (NLS %s)' % (title, nls))
    
    magic_powers = magic_powers.replace('uniquement', '')
    magic_powers = re.sub(r'\bniveau\b', 'lvl', magic_powers)
    magic_powers = re.sub(r'\bniveaux\b', 'lvl', magic_powers)
    
    for magic_power in magic_powers.splitlines():
        nb_by_day, spells = magic_power.split('—')
        contents.append('property | %s | %s' % (nb_by_day, spells))


def format_senses(text):
    for k, v in SENSES.items():
        text = re.sub(r'\b%s\b' % k, v, text)
    return text


def format_feats(contents, summon: Monster):
    text = summon.feats
    if not summon.feats:
        return
    text = ', '.join([f2 for i in text.split(',') if (f2 := (f[:-1] if (f := i.strip()).endswith('B') else f)) and f2 not in FEATS_FILTER and not f2.startswith('Talent (')])
    if text:
        contents.append('property | Dons: |%s ' % text)


def format_pv(text):
    m = re.search(r'(\d+) \((\d+)d', text)
    pv = m.group(1)
    dv = m.group(2)
    return '%s [%s]' % (text, int(pv) + int(dv) * 2)


def format_saving_throw(text):
    value = int(text.replace('+', ''))
    return '%s [+%s]' % (text, value + 2)


def format_bmo(text):
    s_text = text.split()
    value = int(s_text[0].replace('+', ''))
    value_boost = value + 2
    if value_boost >= 0:
        value_boost = '+%s' % value_boost
    if len(s_text) > 1:
        lutte_val = s_text[1][1:]
        lutte_val_boost = int(lutte_val.replace('+', '')) + 2
        
        return '%s [%s] (%s:%s [+%s])' % (s_text[0], value_boost, ACTION['lutte'], lutte_val, lutte_val_boost)
    else:
        return '%s [%s]' % (text, value_boost)


def format_dnd(text):
    s_text = text.split()
    
    if len(s_text) > 1:
        vs_cej_val = s_text[1][1:]
        if 'impossible' in text:
            vs_cej_val = '🛇'
        
        return '%s (%s:%s)' % (s_text[0], ACTION['croc-en-jambe'], vs_cej_val)
    else:
        return text


def format_jda(contents, summon: Monster, stats_and_mod):
    if summon.cac:
        bba = int(summon.bba.replace('+', ''))
        mod_size = MOD_SIZE[summon.size]
        mod_str = stats_and_mod[0][1]
        
        if summon.feats and 'Attaque en finesse' in summon.feats:
            mod_str = stats_and_mod[1][1]
        
        if mod_str == '-':
            bjda = bba + mod_size
            if bjda >= 0:
                bjda = '+%s' % bjda
            contents.append('property| JdA: | %s' % bjda)
        else:
            bjda = bba + mod_size + mod_str
            if summon.feats and 'Attaque en finesse' in summon.feats:
                bjda2 = bba + mod_size + stats_and_mod[0][1] + 2
            else:
                bjda2 = bjda + 2
            
            if bjda >= 0:
                bjda = '+%s' % bjda
            if bjda2 >= 0:
                bjda2 = '+%s' % bjda2
            contents.append('property| JdA: | %s [%s]' % (bjda, bjda2))


def format_jda_dist(contents, summon: Monster, stats_and_mod):
    if summon.distance:
        bba = int(summon.bba.replace('+', ''))
        mod_size = MOD_SIZE[summon.size]
        mod_dex = stats_and_mod[1][1]
        if mod_dex == '-':
            bjda = bba + mod_size
            if bjda >= 0:
                bjda = '+%s' % bjda
        else:
            bjda = bba + mod_size + mod_dex
            if bjda >= 0:
                bjda = '+%s' % bjda
        
        if summon.cac:
            contents[-1] = contents[-1] + ' <b>Dist:</b> %s' % bjda
        else:
            contents.append('property| JdA: | <b>Dist:</b> %s' % bjda)


def format_special(contents, summon: Monster, special=False):
    if not summon.special_capa_info:
        return
    text = summon.special_capa_info
    
    if 'Nage dans la terre' in summon.special_capa_info:
        text = '''
        Nage dans la terre (Ext) Lorsqu'un élémentaire de Terre se déplace par creusement, il peut traverser la pierre, la terre et quasiment n'importe quel autre type de sol (mais pas le métal) aussi facilement que s'il était un poisson nageant dans l'eau. Il ne crée pas de vibration ni aucun autre signe trahissant sa présence.
        Maîtrise de la Terre (Ext) Lorsqu'un élémentaire touche le sol et attaque une cible en contact avec le sol, il bénéficie d'un bonus de +1 aux jets d'attaque et de dégâts. Contre un adversaire volant ou dans l'eau, l'élémentaire subit un malus de -4 aux jets d'attaque et de dégâts.
        '''
    if 'Extinction des feux' in summon.special_capa_info:
        text = '''
        Extinction des feux  (Ext)  D'un simple contact il peut éteindre les feux non magiques de taille ≤ G, si le feux est magiques il l'éteint comme avec dissipation de la magie NLS=DV.
        Maîtrise de l'Eau (Ext) Un élémentaire de l'Eau gagne un bonus de +1 aux jets d'attaque et de dégâts si lui et son adversaire sont en contact avec de l'eau. Si l'élementaire ou son adversaire touche le sol, il subit un malus de -4 aux jets d'attaque et de dégâts.
        Vortex  (Sur) Cette capacité fonctionne comme l'attaque spéciale de tourbillon mais le tourbillon doit être créé dans l'eau et ne peut pas en sortir.
        '''
    if 'Combustion' in summon.special_capa_info:
        text = '''
        Combustion (Ext)  DD:12 (10 + 1/2DV + mod Con).
        L’adversaire doit réussir un jet de Réflexes pour éviter de prendre feu et de subir <b>2d6</b> pendant <b>1d4</b> rounds au début de son tour.
        Si l’adversaire prend feu, il peut tenter un nouveau JdS mais cela lui prend une action complexe. S’il se couche et roule au sol -> bonus de +4 à ce jet de sauvegarde.
        Lorsqu’un adversaire touche avec une arme naturelle ou une attaque à mains nues : il subit des dégâts de feu & doit faire un jet de Réflexes pour éviter de prendre feu.
        '''
    if 'Gestalt' in summon.special_capa_info:
        text = '''
        Gestalt (Sur) Par une action complexe, un groupe de 9 archons peut fusionner pour 2d4 rounds en une entité de taille G. Il possède tous les pouvoirs et capacités d'un élém de l'air de taille G ainsi que les suivantes : 2 rayons de lumière (2d6) ; RD 5/Mal et magie ; sous-type des archons, du Bien et de la Loi ; traits des archons (Aura de menace DD 16). Lorsque le gestalt se séparent, les PV restant sont divisés en parts égales entre eux.'''
    
    if 'Changement de forme' in summon.special_capa_info:
        text = '''
        Changement de forme (Sur) Un archon canin peut prendre n'importe quelle forme canine de taille P et G, comme s'il utilisait forme bestiale II. Une fois cette forme, l'archon perd ses attaques et obtient l'attaque de morsure de la forme choisie.
        '''
    if 'Nuage d\'encre' in summon.special_capa_info:
        text = '''
        Nuage d'encre (Ext) Par une action libre utilisable x1/min, la créature peut produire une sphère d'encre de 9m(6c) de rayon. L'encre offre un camouflage total dans l'eau et reste active pendant 1 min.
        '''
    if 'Mucus protecteur' in summon.special_capa_info:
        text = '''
        Mucus protecteur (Sur) Si une créature ou une arme touche elle subit 1d8 dgm %s si elle rate un jet de Réflexes de DD 18. Si la solidité de l'arme est vaincu elle devient brisée. Les flèches et autres projectiles qui infligent des dgm normaux puis sont détruits.
        ''' % ENERGIES_TYPE['acide']
    
    if summon.name == 'Crocodile sanguinaire':
        text = text.replace('Tout en restant attaché à sa victime, il recroqueville ses pattes puis tourne rapidement sur lui-même, agitant sa prise dans tous les sens.', '')
    
    if summon.name in ('Élémentaire de la Terre noble', 'Seigneur élémentaire de la Terre'):
        text = '''Nage dans la terre (Ext)
        Maîtrise de la Terre (Ext)
        '''
    if summon.name in ('Élémentaire du Feu noble', 'Seigneur élémentaire du Feu'):
        text = '''Combustion (Ext)
        '''
    if summon.name in 'Seigneur élémentaire de l\'Eau':
        text = '''Extinction des feux (Ext)
        Maîtrise de l'Eau (Ext)
        Vortex (Sur)
        '''
    
    if summon.name == 'Kyton':
        text = '''
        Armure de chaînes (Ext) Les chaînes qui recouvrent les kytons leur donnent un bonus d'armure de +4 et n'imposent aucune pénalités.
        Danse des chaînes (Sur) Par une AS, un kyton peut contrôler jusqu'à 4 chaînes dans un rayon de 6m(4c) et les faire se déplacer selon ses désirs. Il peut allonger ces chaînes d'une distance max de 4,50m(3c) et les doter de barbelés acérés. Les chaînes attaquent avec la même efficacité que le kyton. Si une des chaînes est possédée par une autre créature, elle peut effectuer un jet de Volonté de DD 15 pour briser le pouvoir que le kyton exerce sur la chaîne. En cas de JdS réussi, le kyton ne peut plus contrôler cette chaîne pendant 2 h à moins que la créature l'ait abandonnée. Le kyton peut escalader les chaînes qu'il contrôle en se déplaçant à sa vitesse normale et sans test d'Escalade. Le DD du JdS dépend du Cha.
        Regard déstabilisant (Sur) Portée 9m(6c), Volonté DD 15 annule. Un kyton peut modeler son visage pour qu'il ressemble à celui d'un mort qui était un être aimé ou un ennemi juré de son adversaire. Si une victime rate son JdS, elle est secouée pendant 1d3 rd. Il s'agit d'un effet mental de terreur. Le DD dépend du Cha.
        '''
    if summon.name == 'Démon des ombres':
        text = '''
        Fusion avec les ombres (Sur) Sous n'importe quelle condition de luminosité autre qu'une lumière vive, un démon des ombres peut se fondre dans les ombres par une action de mouvement (comme s'il devenait invisible). Les lumières artificielles et les sorts de lumière de lvl ⩽ à 2 ne contre pas cette capacité.
        Sprint (Ext) Une fois par min, un démon des ombres peut augmenter sa vitesse de vol à 36m(48c) pendant 1 rd.
        Impuissant sous la lumière du soleil (Ext) Les démons des ombres sont impuissants sous une lumière vive ou sous le lumière naturelle du soleil et ils fuient ces conditions de luminosité. S'ils ne peuvent s'en extraire, ils sont incapables d'attaquer et ne peuvent effectuer qu'une action de mouvement ou une AS par rd. Les démons des ombres qui possèdent une créature grâce à leur pouvoir de possession ne sont pas affectés par la lumière du soleil mais si un sort de rayon de soleil ou d'explosion de lumière les touche, ils sont expulsés de leur hôte.
        '''
    if summon.name == 'Vrock':
        text = '''
        Cri étourdissant (Sur) Une fois par heure, un vrock peut émettre un cri perçant. Toutes les créatures autres que des démons situées dans une zone de 9 m (6 c) doivent réussir un jet de Vigueur de DD 21 pour éviter d'être étourdies pendant 1 round. Le DD dépend de la Constitution.
        Spores (Ext) Via une action libre un vrock peut relâcher un nuage de spores une fois tous les 3 rounds. Les créatures adjacentes subissent 1d8 points de dégâts causés par les spores, plus 1d4 points de dégâts par round pendant 10 rounds lorsque les spores grandissent en de fines racines. Bien que très laides, les racines sont inoffensives et disparaissent en 1d4 jours si elles ne sont pas retirées. Les spores peuvent être détruites par le sort bénédiction, avec de l'eau bénite, les effets qui guérissent la maladie ou immunisent contre elle.
        Danse de la destruction (Sur) Un vrock peut danser et chanter au prix d'une action complexe - à la fin de 3 rounds passés à danser, une vague d'énergie explose autour du vrock, infligeant 5d6 points de dégâts d'électricité à toutes les créatures dans les 30 m (20 c). Un jet de Réflexes DD 17 ½ dégâts. Pour chaque vrock supplémentaire qui se joint à la danse, les dégâts augmentent de 5d6 et le DD du jet de Réflexes augmente de +1, jusqu'à un maximum de 20d6 pour 4 ou plus vrocks (le DD continue d'augmenter avec les vrocks mais pas les dégâts). La danse s'interrompt et doit être de nouveau initiée si un des participants est tué ou empêché de danser. Le DD du jet de sauvegarde est basé sur le Charisme.
        '''
    if summon.name == 'Succube':
        text = '''
        Don de malfaisance (Sur) Une fois par jour, une succube peut utiliser une action complexe pour octroyer un don de malfaisance à un humanoïde volontaire en restant au contact avec lui pendant un round entier. La cible gagne alors un bonus de malfaisance de +2 à une caractéristique de son choix. La succube ne peut accorder qu'un seul don de malfaisance à la fois à une créature donnée. Tant que le don de malfaisance persiste, la succube peut communiquer par télépathie avec la cible, quelle que soit la distance qui les sépare (et elle peut utiliser son pouvoir magique de suggestion via ce lien). Un rejet du Mal ou un rejet du Chaos permet de supprimer un don de malfaisance. La succube peut également l'annuler par une action libre (ce qui inflige une diminution permanente de 2d6 points de Charisme à la victime, sans jet de sauvegarde).
        Absorption d'énergie (Sur) Le baiser d'une succube inflige un niveau négatif et possède les mêmes effets qu'un sort de suggestion demandant d'accepter un acte passionné tel qu'un baiser. Les victimes non volontaires doivent être agrippées. La victime doit réussir un jet de Volonté de DD 22 pour résister à cette suggestion. Le DD du jet de Vigueur pour enlever un niveau négatif est également de 22. Ces DD dépendent du Charisme.
        '''
    if summon.name == 'Ghaéle' and not special:
        return
    
    text = text.replace('encore qu\'elle entre en contact avec un chien gobelin pour toute autre raison (en tentant de l\'agripper ou de le monter par exemple)', 'encore qu\'elle entre en contact pour toute autre raison')
    
    text = base_replace_in_desc(text)
    
    contents.append('section | Capacités spécial')
    
    for line in text.splitlines():
        line = line.strip()
        if not line:
            continue
        if '(Ext)' in line or '(Ex)' in line:
            if '(Ext)' in line:
                cs_name, cs_desc = line.split('(Ext)')
            else:
                cs_name, cs_desc = line.split('(Ex)')
            cs_name = cs_name.strip()
            cs_desc = cs_desc.strip()
            if cs_desc.startswith('.'):
                cs_desc = cs_desc[1:]
            cs_desc = cs_desc.strip()
            if cs_name.startswith('Venin'):
                cs_name = cs_name.replace('Venin', ENERGIES_TYPE['poison'] + 'Venin')
            if cs_name.startswith('Poison'):
                cs_name = cs_name.replace('Poison', ENERGIES_TYPE['poison'] + 'Poison')
            if cs_name.startswith('Maladie'):
                cs_name = cs_name.replace('Maladie', ENERGIES_TYPE['maladie'] + 'Maladie')
            
            if cs_desc:
                line = '<b>%s (Ext):</b> %s' % (cs_name, cs_desc)
            else:
                line = '<b>%s (Ext)</b>' % cs_name
        if '(Sur)' in line:
            cs_name, cs_desc = line.split('(Sur)')
            cs_name = cs_name.strip()
            cs_desc = cs_desc.strip()
            if cs_desc.startswith('.'):
                cs_desc = cs_desc[1:]
            cs_desc = cs_desc.strip()
            
            if cs_desc:
                line = '<b>%s (Sur):</b> %s' % (cs_name, cs_desc)
            else:
                line = '<b>%s (Sur)</b>' % cs_name
        
        contents.append('text | %s ' % line)


def format_attack_special(text):
    text = ', '.join([i.strip().capitalize() for i in text.split(',')])
    text = re.sub(r'\b[D,d]d\b', 'DD', text)
    text = re.sub(r'\b[P,p][V,v]\b', 'PV', text)
    text = format_energies_type(text)
    text = format_actions(text)
    return text


def format_illustration(contents, summon: Monster, in_extra_card=False):
    if not WITH_ILLU:
        return
    image_info = SUMMON_INFO[summon.name]['image']
    if not in_extra_card and not image_info['is_display']:
        return
    
    if in_extra_card and not image_info['in_extra']:
        return
    
    if image_info['need_end_card']:
        contents.append('end_card')
    contents.append('picture| %s | 100%%' % image_info['url'])


def build_content(summon: Monster):
    contents = [
        'section | Défense',
        'property | |<b>CA:</b> %s-%s-%s <b>PV:</b> %s' % (summon.ca, summon.ca_touch, summon.ca_surprised, format_pv(summon.pv)),
        'property | |<b>Réf:</b> %s <b>Vig:</b> %s <b>Vol:</b> %s' % (summon.saving_throw_ref, format_saving_throw(summon.saving_throw_vig), summon.saving_throw_vol)
    ]
    
    stats_and_mod = compute_stats_and_mod(summon)
    
    if summon.capa_def:
        text = summon.capa_def
        text = ', '.join([i.strip().capitalize() for i in text.split(',')])
        contents.append('property | CD: |%s' % text)
    
    if summon.rd and summon.rm:
        contents.append('property | RD: |%s <b>RM:</b> %s' % (summon.rd, summon.rm))
    else:
        if summon.rd:
            contents.append('property | RD: |%s' % summon.rd)
        if summon.rm:
            contents.append('property | RM:|%s' % summon.rm)
    format_jds(contents, summon)
    if summon.weakness:
        contents.append('property | Faiblesses:|%s' % format_energies_type(summon.weakness))
    if summon.immunity:
        contents.append('property | Immunités: |%s' % format_energies_type(summon.immunity).replace('traits des élémentaires', 'Traits des élémentaires'))
    if summon.resistance:
        contents.append('property | Résistances: |%s' % format_energies_type(summon.resistance))
    
    # contents.append('fill')
    contents.append('section | Attaque')
    contents.append('property | VD: |%s' % format_vd(summon))
    
    format_attack(contents, summon.cac, 'CàC')
    format_attack(contents, summon.distance, 'Dist')
    
    format_jda(contents, summon, stats_and_mod)
    format_jda_dist(contents, summon, stats_and_mod)
    
    if summon.attack_special:
        contents.append('property | Spé: |%s' % format_attack_special(summon.attack_special))
    
    # contents.append('fill')
    contents.append('section | Info')
    format_stats(contents, summon)
    contents.append('property | FP: | %s <b>Taille:</b> %s <b>Align:</b> %s <b>Init:</b> %s' % (summon.fp, summon.size, summon.raw_align, summon.init))
    
    if '(' in summon.bmo:
        contents.append('property | BBA: |%s <b>BMO:</b> %s' % (summon.bba, format_bmo(summon.bmo)))
        contents.append('property | DMD: | %s' % format_dnd(summon.dmd))
    else:
        contents.append('property | BBA: |%s <b>BMO:</b> %s <b>DMD:</b> %s' % (summon.bba, format_bmo(summon.bmo), format_dnd(summon.dmd)))
    format_feats(contents, summon)
    
    if summon.senses:
        contents.append('property | Senses: | %s' % format_senses(summon.senses))
    if summon.skill:
        contents.append('property | Comp: |%s ' % format_skill(summon.skill))
    if summon.special:
        contents.append('property | CS: |%s ' % summon.special.capitalize())
    if summon.languages:
        text = summon.languages
        text = ', '.join([i.strip().capitalize() for i in text.split(',')])
        contents.append('property | Lang: |%s ' % text)
    
    if not SUMMON_INFO[summon.name]['extra_card']:
        format_special(contents, summon)
        format_magic_powers(contents, summon.magic_powers, summon.nls, 'Pouvoirs magiques')
        format_magic_powers(contents, summon.spells, summon.spell_lvl, 'Sorts préparés')
    
    format_illustration(contents, summon)
    # contents.append('fill')
    return contents


def build_title_size(summon):
    title_size = None
    if summon.name.startswith('Élémentaire'):
        title_size = 10
    if summon.name.startswith('Seigneur élémentaire'):
        title_size = 10
    if summon.name.startswith('Fourmi géante'):
        title_size = 11
    if summon.name == 'Chauve-souris sanguinaire':
        title_size = 10
    if summon.name == 'Archon lumineux':
        title_size = 11
    if summon.name == 'Diable des glaces':
        title_size = 11
    if summon.name == 'Diable barbelé':
        title_size = 12
    if summon.name == 'Démon des ombres':
        title_size = 11
    if summon.name == 'Molosse infernal':
        title_size = 11
    if summon.name == 'Crocodile sanguinaire':
        title_size = 11
    if summon.name == 'Archon messager':
        title_size = 12
    if summon.name == 'Serpent constricteur':
        title_size = 12
    if summon.name == 'Sanglier sanguinaire':
        title_size = 12
    
    return title_size


def build_name(summon):
    if summon.align == 'good':
        name = '%s - 👼 - %s' % (TO_ROMAN[int(summon.level)], summon.name)
    elif summon.align == 'bad':
        name = '%s - 😈 - %s' % (TO_ROMAN[int(summon.level)], summon.name)
    else:
        name = '%s - %s' % (TO_ROMAN[int(summon.level)], summon.name)
    name = name.replace('Élémentaire de la Terre', 'Élémentaire de Terre')
    name = name.replace('Seigneur élémentaire', 'Seigneur élém')
    name = name.replace('de taille', '-')
    return name


def build_copyright(summon: Monster = None, mephit: Mephit = None):
    copy_img = '<img src="img/Pathfinder-Logo-600x257.png" style=" position: relative;    top: -6px; width: 56px;    height: 24px;"> '
    if summon:
        return copy_img + '<span style="text-align: right;width: 100%%;">Invocation %s - %s/%s</span>' % (TO_ROMAN[int(summon.level)], summon.index, summon.total)
    elif mephit:
        return copy_img + '<span style="text-align: right;width: 100%%;">Invocation %s - Méphite - %s/%s</span>' % (TO_ROMAN[int(4)], mephit.index, mephit.total)
    else:
        return copy_img + '<span style="text-align: right;width: 100%;">Invocation</span>'


def build_card(summon: Monster):
    name = build_name(summon)
    level = summon.level
    contents = build_content(summon)
    card_copyright = build_copyright(summon)
    title_size = build_title_size(summon)
    icon = SUMMON_INFO[summon.name]['icon']
    
    card = build_card_dict(name, contents, level, card_copyright, icon, title_size)
    return card


def build_extra_card(summon: Monster):
    if not SUMMON_INFO[summon.name]['extra_card']:
        return
    
    name = build_name(summon)
    level = summon.level
    contents = []
    
    format_special(contents, summon)
    format_magic_powers(contents, summon.magic_powers, summon.nls, 'Pouvoirs magiques')
    format_magic_powers(contents, summon.spells, summon.spell_lvl, 'Sorts préparés')
    format_illustration(contents, summon, in_extra_card=True)
    
    card_copyright = build_copyright(summon)
    title_size = build_title_size(summon)
    icon = SUMMON_INFO[summon.name]['icon']
    
    card = build_card_dict(name, contents, level, card_copyright, icon, title_size)
    return card


def build_mephit_card(mephit: Mephit):
    name = mephit.name
    level = 4
    contents = []
    
    if mephit.desc:
        contents.append('text|%s' % mephit.desc)
        contents.append('text|')
    
    if mephit.breath:
        contents.append('property | Souffle: | %s' % base_replace_in_desc(mephit.breath))
    if mephit.fast_healing:
        contents.append('property | Guérison accélérée: | %s' % mephit.fast_healing)
    if mephit.vd:
        contents.append('property | VD: | %s' % mephit.vd)
    if mephit.immunity:
        contents.append('property | Immunité: | %s' % format_energies_type(mephit.immunity))
    if mephit.weakness:
        contents.append('property | Faiblesse: | %s' % format_energies_type(mephit.weakness))
    
    if mephit.magic_powers:
        contents.append('text|')
        contents.append('section | Pouvoirs magiques')
        text = base_replace_in_desc(mephit.magic_powers)
        for line in text.split(','):
            contents.append('bullet | %s' % line)
    
    if mephit.capa:
        contents.append('text|')
        contents.append('section | Capacités spécial')
        contents.append('text | %s' % base_replace_in_desc(mephit.capa))
    
    card_copyright = build_copyright(mephit=mephit)
    title_size = None
    icon = MEPHIT_INFO[mephit.name]['icon']
    
    card = build_card_dict(name, contents, level, card_copyright, icon, title_size)
    return card


def main():
    cards_help = build_cards_help()
    all_cards = []
    all_cards.extend(cards_help)
    for summon_level in range(1, 10):
        if not os.path.exists(SUMMON_PATH % summon_level):
            print('%s is missing' % (SUMMON_PATH % summon_level))
            return
        
        summons = read_summon_file(summon_level)
        
        cards = []
        for summon in summons:
            card = build_card(summon)
            cards.append(card)
            all_cards.append(card)
            extra_card = build_extra_card(summon)
            if extra_card:
                cards.append(extra_card)
                all_cards.append(extra_card)
            if summon.name == 'Ghaéle':
                name = build_name(summon)
                level = summon.level
                contents = []
                
                format_special(contents, summon, special=True)
                card_copyright = build_copyright(summon)
                title_size = build_title_size(summon)
                icon = SUMMON_INFO[summon.name]['icon']
                
                card = build_card_dict(name, contents, level, card_copyright, icon, title_size)
                all_cards.append(card)
            # print(card)
        
        if summon_level == 1:
            cards.extend(cards_help)
        
        with open('out/cards-summon-lvl-%s.json' % summon_level, 'w', encoding='utf-8') as fd:
            json.dump(cards, fd)
    
    mephits = read_mephit_file()
    mephit_cards = []
    for mephit in mephits:
        mephit_card = build_mephit_card(mephit)
        mephit_cards.append(mephit_card)
        all_cards.append(mephit_card)
    
    with open('out/cards-summon-mephit.json', 'w', encoding='utf-8') as fd:
        json.dump(mephit_cards, fd)
    
    with open('out/cards-summon.json', 'w', encoding='utf-8') as fd:
        json.dump(all_cards, fd)


if __name__ == '__main__':
    main()
