#!/usr/bin/python
# -*- coding: utf-8 -*-

import csv
import os

from bs4 import BeautifulSoup

from common import get_page, format_distance
from make_spell_card import PjClass, my_pj_class, get_level

page_by_class = {
    PjClass.Conj: 'https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Liste%20des%20sorts%20de%20conjurateurs.ashx',
    PjClass.Pre : 'https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Liste%20des%20sorts%20de%20pr%C3%AAtres.ashx',
    PjClass.Mag : 'https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Liste%20des%20sorts%20de%20magus.ashx',
}


def found_all_spell_links():
    spell_links = []
    page = get_page(page_by_class[my_pj_class])
    soup = BeautifulSoup(page, 'html.parser')
    # print(soup.prettify())
    found_spell = False
    for i in list(soup.find(id='PageContentDiv')):
        if i.text.startswith('Sorts de niveau 0'):
            found_spell = True
        if i.text.startswith('Sorts de niveau'):
            print()
            print(i.text[:-1])
            print('-----------------------')
        if found_spell and i.name == 'ul':
            for j in i.find_all('li'):
                print(j.a.text)
                spell_links.append(j.a['href'])
    return spell_links


class Spell:
    name = ''
    link = ''
    info = ''
    speciality = ''
    school = ''
    level = ''
    index = ''
    my_level = ''
    total_by_level = ''
    casting_time = ''
    components = ''
    range = ''
    effect = ''
    area_effect = ''
    target = ''
    duration = ''
    saving_throw = ''
    magic_resistance = ''
    description = []
    description_mythic = []
    
    
    def __init__(self):
        self.name = ''
        self.link = ''
        self.info = ''
        self.speciality = ''
        self.school = ''
        self.level = ''
        self.index = ''
        self.my_level = ''
        self.total_by_level = ''
        self.casting_time = ''
        self.components = ''
        self.range = ''
        self.effect = ''
        self.area_effect = ''
        self.target = ''
        self.duration = ''
        self.saving_throw = ''
        self.magic_resistance = ''
        self.description = []
        self.description_mythic = []
    
    
    def __str__(self):
        return '\n'.join(['%s : %s' % (k, v) for (k, v) in self.__dict__.items() if not k.startswith('_')])
    
    
    def __repr__(self):
        return self.__str__()
    
    
    def _format(self):
        self.description = '\n'.join(self.description)
        self.description_mythic = '\n'.join(self.description_mythic)


def parse_spell_page(link) -> Spell:
    spell = Spell()
    s_link = 'https://www.pathfinder-fr.org/Wiki/%s' % link
    # print(s_link)
    page = get_page(s_link)
    soup = BeautifulSoup(page, 'html.parser')
    
    spell.name = soup.find(id='PageHeaderDiv').h1.text.strip()
    spell.link = s_link
    
    page_content_div = soup.find(id='PageContentDiv')
    # print(page_content_div)
    
    for elem in page_content_div.find_all(['br']):
        elem.append('$NEW_LINE$')
    for elem in page_content_div.find_all(['h2']):
        elem.insert(0, '$NEW_LINE$')
        elem.append('$NEW_LINE$')
    
    raw_speciality = page_content_div.find('div', {'class': 'navmenudroite'})
    # print(raw_speciality)
    if raw_speciality:
        spell.speciality = raw_speciality.text.replace('$NEW_LINE$', ' ').strip()
        raw_speciality.clear()
    
    # print(page_content_div.text)
    full_text = page_content_div.text
    full_text = format_distance(full_text)
    full_text = ''.join(full_text.splitlines())
    full_text = full_text.replace('$NEW_LINE$', '\n')
    text_page = list(full_text.splitlines())
    # print(text_page)
    # if 'Temps d\'incantation' in text_page[0]:
    #     line_0, line_1 = text_page[0].split('Temps d\'incantation')
    #     line_1 = 'Temps d\'incantation ' + line_1
    #     text_page.pop(0)
    #     text_page.insert(0, line_1)
    #     text_page.insert(0, line_0)
    
    description_mythic = False
    
    # print(text_page)
    while text_page:
        line = text_page.pop(0).strip()
        line = line.replace('’', '\'')
        if not line:
            continue
        
        if not spell.school and 'École' in line:
            line = line.replace(':', '')
            if not line.startswith('École'):
                info, line = line.split('École', 1)
                line = 'École' + line
                spell.info = info.strip()
            
            if 'Niveau' in line:
                school, level = line.split('Niveau')
                spell.school = school.strip()[len('École'):-1].strip()
                spell.level = level.strip()
            else:
                spell.school = line.strip()[len('École'):].strip()
            continue
        if not spell.level and line.startswith('Niveau'):
            line = line.replace(':', '')
            spell.level = line.strip()[len('Niveau'):].strip()
            continue
        if not spell.casting_time and line.startswith('Temps d\'incantation'):
            line = line.replace(':', '')
            spell.casting_time = line[len('Temps d\'incantation'):].strip()
            continue
        if not spell.components and line.startswith('Composantes'):
            line = line.replace(':', '')
            spell.components = line[len('Composantes'):].strip()
            continue
        if not spell.components and line.startswith('Composantes'):
            line = line.replace(':', '')
            spell.components = line[len('Composantes'):].strip()
            continue
        if not spell.range and line.startswith('Portée'):
            line = line.replace(':', '')
            spell.range = line[len('Portée'):].strip()
            continue
        if not spell.effect and line.startswith('Effet'):
            line = line.replace(':', '')
            spell.effect = line[len('Effet'):].strip()
            continue
        if not spell.area_effect and line.startswith('Zone d\'effet'):
            line = line.replace(':', '')
            spell.area_effect = line[len('Zone d\'effet'):].strip()
            continue
        if not spell.target and line.startswith('Cible'):
            line = line.replace(':', '')
            if line.startswith('Cibles'):
                line = 'Cible' + line[len('Cibles'):]
            if 'Durée' in line:
                target, duration = line.split('Durée')
                spell.target = target.strip()[len('Cible'):].strip()
                spell.duration = duration.strip()
            else:
                spell.target = line[len('Cible'):].strip()
            continue
        if not spell.duration and line.startswith('Durée'):
            line = line.replace(':', '')
            spell.duration = line[len('Durée'):].strip()
            continue
        if not spell.saving_throw and line.startswith('Jet de sauvegarde'):
            line = line.replace(':', '')
            if 'Résistance à la magie' in line:
                saving_throw, magic_resistance = line.split('Résistance à la magie')
                spell.saving_throw = saving_throw.strip()[len('Jet de sauvegarde'):-1].strip()
                spell.magic_resistance = magic_resistance.strip()
            else:
                spell.saving_throw = line[len('Jet de sauvegarde'):].strip()
            continue
        if not spell.magic_resistance and line.startswith('Résistance à la magie'):
            line = line.replace(':', '')
            spell.magic_resistance = line[len('Résistance à la magie'):].strip()
            continue
        
        # print(f'line [{line}]')
        
        if line.strip() == 'Mythique¶':
            description_mythic = True
            continue
        
        if description_mythic:
            spell.description_mythic.append(line)
        else:
            spell.description.append(line)
    
    spell._format()  # noqa
    return spell


def write_file(spells, level):
    file_name = 'out/spells-%s.csv' % my_pj_class.name if level is None else 'out/spells-%s-lvl-%s.csv' % (my_pj_class.name, level)
    with open(file_name, 'w', newline='', encoding='utf-8') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=[k for k in Spell.__dict__.keys() if not k.startswith('_')])
        writer.writeheader()
        for spell in spells:
            writer.writerow(spell.__dict__)


def main():
    if not os.path.exists('out'):
        os.makedirs('out')
    if not os.path.exists('cache'):
        os.makedirs('cache')
    
    # spell = parse_spell_page('Pathfinder-RPG.Fouet%20daraign%c3%a9es.ashx')
    
    spells = []
    spell_links = found_all_spell_links()
    for spell_link in spell_links:
        spell = parse_spell_page(spell_link)
        spells.append(spell)
    
    cards_count_by_level = {i: [] for i in range(10)}
    for spell in spells:
        level = get_level(spell, my_pj_class)
        spell.my_level = level
        cards_count_by_level[level].append(spell)
        spell.index = len(cards_count_by_level[level])
    
    for spell in spells:
        spell.total_by_level = len(cards_count_by_level[spell.my_level])
        # print(spell)
    
    for sp_by_lvl, spell_lvl in cards_count_by_level.items():
        write_file(spell_lvl, sp_by_lvl)
    
    write_file(spells, None)
    
    print('DONE')


if __name__ == '__main__':
    main()
