#!/usr/bin/python
# -*- coding: utf-8 -*-

import csv
import os
from enum import Enum

from bs4 import BeautifulSoup

from common import get_page, format_distance


class Align(Enum):
    bien = 'good'
    mal = 'bad'
    perso = '*'


class Mephit(object):
    name = None
    elem_type = None
    index = None
    total = None
    vd = None
    breath = None
    magic_powers = None
    fast_healing = None
    desc = None
    immunity = None
    weakness = None
    capa = None
    
    
    def __str__(self):
        return '\n'.join(['%s : %s' % (k, v) for (k, v) in self.__dict__.items() if not k.startswith('_')])
    
    
    def __repr__(self):
        return self.__str__()


class Monster(object):
    name = ''
    sub_name = ''
    index = ''
    total = ''
    link = ''
    align = ''
    level = ''
    fp = ''
    px = None
    size = None
    raw_align = None
    init = None
    senses = None
    perception = None
    ca = None
    ca_touch = None
    ca_surprised = None
    pv = None
    saving_throw_ref = None
    saving_throw_vig = None
    saving_throw_vol = None
    saving_throws = None
    capa_def = None
    immunity = None
    rd = None
    resistance = None
    rm = None
    weakness = None
    vd = None
    cac = None
    space = None
    reach = None
    attack_special = None
    distance = None
    nls = None
    magic_powers = []
    spell_lvl = None
    spells = []
    stats = None
    bba = None
    bmo = None
    dmd = None
    feats = None
    skill = None
    special = None
    languages = None
    environment = None
    social_organization = None
    treasure = None
    special_capa_info = []
    
    
    def __init__(self):
        self.magic_powers = []
        self.spells = []
        self.special_capa_info = []
    
    
    def __str__(self):
        return '\n'.join(['%s : %s' % (k, v) for (k, v) in self.__dict__.items() if not k.startswith('_')])
    
    
    def __repr__(self):
        return self.__str__()
    
    
    def _format(self):
        self.magic_powers = '\n'.join(self.magic_powers)
        self.spells = '\n'.join(self.spells)
        self.special_capa_info = '\n'.join(self.special_capa_info)


def parse_align(sous_type):
    if sous_type == '—':
        return Align.perso.value
    elif 'Bien' in sous_type:
        return Align.bien.value
    elif 'Mal' in sous_type:
        return Align.mal.value
    return Align.perso.value


def parse_link(name):
    return 'https://www.pathfinder-fr.org/Wiki/' + name.a['href']


def parse_name(name):
    name = name.text
    name = name.replace('*', '')
    name = name.replace(' (diable)', '')
    name = name.replace(' (démon)', '')
    name = name.replace(' (félin)', '')
    name = name.replace(' (animal grégaire)', '')
    name = name.replace(' (dinosaure)', '')
    name = name.replace(' (baleine)', '')
    name = name.replace(' (ange)', '')
    name = name.replace(' (éléphant)', '')
    name = name.replace(' (tous)', '')
    name = name.replace(' (azata)', '')
    name = name.replace('Crapaud venimeux', 'Grenouille venimeuse')
    return name


def helper(monsters, name, lvl, link, special_capa_info, languages):
    monster = Monster()
    monster.name = name
    monster.align = Align.perso.value
    monster.level = lvl
    monster.link = link
    monsters[monster.level].append(monster)
    monster.index = len(monsters[monster.level])
    monster.special_capa_info = special_capa_info
    monster.languages = languages


def build_elem(monsters):
    special_capa_info = [
        '''Nage dans la terre (Ext). Lorsqu'un élémentaire de Terre se déplace par creusement, il peut traverser la pierre, la terre et quasiment n'importe quel autre type de sol (mais pas le métal) aussi facilement que s'il était un poisson nageant dans l'eau. S'il est protégé contre les dégâts de feu, il peut même traverser la lave. Il ne laisse aucun tunnel et aucune ouverture dans son sillage. Il ne crée pas de vibration ni aucun autre signe trahissant sa présence. Un sort de glissement de terrain lancé sur la zone à l'intérieur de laquelle se trouve l'élémentaire de Terre le projette à 9 m (6 c) et l'étourdit pendant 1 round (à moins qu'il ne réussisse un jet de Vigueur de DD 15).''',
        '''Maîtrise de la terre (Ext). Lorsqu'un élémentaire touche le sol et attaque une cible en contact avec le sol, il bénéficie d'un bonus de +1 aux jets d'attaque et de dégâts. Contre un adversaire volant ou dans l'eau, l'élémentaire subit un malus de -4 aux jets d'attaque et de dégâts. Ces modificateurs s'appliquent également lors des manoeuvres de bousculade et de renversement, que l'élémentaire en soit l'initiateur ou pas. Ces modificateurs ne sont pas comptés dans les profils suivants.''',
    ]
    languages = 'terreux'
    link = 'https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.%C3%89l%C3%A9mentaire%20de%20la%20Terre.ashx'
    helper(monsters, 'Élémentaire de la Terre de taille P', 2, link, special_capa_info, languages)
    helper(monsters, 'Élémentaire de la Terre de taille M', 4, link, special_capa_info, languages)
    helper(monsters, 'Élémentaire de la Terre de taille G', 5, link, special_capa_info, languages)
    helper(monsters, 'Élémentaire de la Terre de taille TG', 6, link, special_capa_info, languages)
    helper(monsters, 'Élémentaire de la Terre noble', 7, link, special_capa_info, languages)
    helper(monsters, 'Seigneur élémentaire de la Terre', 8, link, special_capa_info, languages)
    
    special_capa_info = [
        '''Maîtrise de l'air (Ext) Les créatures volantes subissent un malus de -1 aux jets d'attaque et de dégâts contre un élémentaire de l'Air.''',
    ]
    languages = 'aérien'
    link = 'https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.%C3%89l%C3%A9mentaire%20de%20lAir.ashx'
    helper(monsters, 'Élémentaire de l\'Air de taille P', 2, link, special_capa_info, languages)
    helper(monsters, 'Élémentaire de l\'Air de taille M', 4, link, special_capa_info, languages)
    helper(monsters, 'Élémentaire de l\'Air de taille G', 5, link, special_capa_info, languages)
    helper(monsters, 'Élémentaire de l\'Air de taille TG', 6, link, special_capa_info, languages)
    helper(monsters, 'Élémentaire de l\'Air noble', 7, link, special_capa_info, languages)
    helper(monsters, 'Seigneur élémentaire de l\'Air', 8, link, special_capa_info, languages)
    
    special_capa_info = [
        '''Extinction des feux (Ext). D'un simple contact, l'élémentaire peut éteindre les feux non magiques de taille inférieure ou égale à G. Il peut dissiper les feux magiques qu'il touche comme s'il utilisait une dissipation de la magie (avec un NLS égal à son nombre de DV).''',
        '''Maîtrise de l'Eau (Ext). Un élémentaire de l'Eau gagne un bonus de +1 aux jets d'attaque et de dégâts si lui et son adversaire sont en contact avec de l'eau. Si lélementaire ou son adversaire touche le sol, il subit un malus de -4 aux jets d'attaque et de dégâts. Ces modificateurs s'appliquent également lors des manœuvres de bousculade et de renversement, que l'élementaire en soit l'initiateur ou pas. Ces modificateurs ne sont pas comptés dans les profils qui suivent.''',
        '''Vortex (Sur). Un élémentaire de l'Eau peut créer un tourbillon par une action simple, à volonté. Cette capacité fonctionne comme l'attaque spéciale de tourbillon (voir la section correspondante) mais le tourbillon doit être créé dans l'eau et ne peut pas en sortir.'''
    ]
    languages = 'aquatique'
    link = 'https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.%C3%89l%C3%A9mentaire%20de%20lEau.ashx'
    helper(monsters, 'Élémentaire de l\'Eau de taille P', 2, link, special_capa_info, languages)
    helper(monsters, 'Élémentaire de l\'Eau de taille M', 4, link, special_capa_info, languages)
    helper(monsters, 'Élémentaire de l\'Eau de taille G', 5, link, special_capa_info, languages)
    helper(monsters, 'Élémentaire de l\'Eau de taille TG', 6, link, special_capa_info, languages)
    helper(monsters, 'Élémentaire de l\'Eau noble', 7, link, special_capa_info, languages)
    helper(monsters, 'Seigneur élémentaire de l\'Eau', 8, link, special_capa_info, languages)
    
    special_capa_info = [
        '''Combustion (Ext). Lorsque les créatures qui disposent de cette capacité touchent un adversaire au corps à corps, elles lui infligent des dégâts de feu en plus des dégâts normaux. L’adversaire doit réussir un jet de Réflexes pour éviter de prendre feu et de subir les dégâts indiqués pendant 1d4 rounds supplémentaires au début de son tour (le DD vaut 10 + la moitié du nombre de DV raciaux de la créature qui incinère + son modificateur de Constitution). Si l’adversaire prend feu, il peut tenter un nouveau jet de sauvegarde par la suite mais cela lui prend une action complexe. S’il se couche et roule au sol, il bénéficie d’un bonus de +4 à ce jet de sauvegarde. Lorsqu’un adversaire touche avec une arme naturelle ou une attaque à mains nues une créature qui dispose de cette capacité, il subit des dégâts de feu comme s’il avait été touché par la créature en question. Il doit également effectuer un jet de Réflexes pour éviter de prendre feu.''',
    ]
    languages = 'igné'
    link = 'https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.%C3%89l%C3%A9mentaire%20du%20Feu.ashx'
    helper(monsters, 'Élémentaire du Feu de taille P', 2, link, special_capa_info, languages)
    helper(monsters, 'Élémentaire du Feu de taille M', 4, link, special_capa_info, languages)
    helper(monsters, 'Élémentaire du Feu de taille G', 5, link, special_capa_info, languages)
    helper(monsters, 'Élémentaire du Feu de taille TG', 6, link, special_capa_info, languages)
    helper(monsters, 'Élémentaire du Feu noble', 7, link, special_capa_info, languages)
    helper(monsters, 'Seigneur élémentaire du Feu', 8, link, special_capa_info, languages)


def build_mephite(monsters):
    special_capa_info = []
    languages = None
    link = 'https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.M%C3%A9phite.ashx'
    helper(monsters, 'Méphite', 4, link, special_capa_info, languages)


def found_all_monsters_links():
    monsters = {i: [] for i in range(1, 10)}
    page = get_page('https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Convocation%20de%20monstres.ashx')
    soup = BeautifulSoup(page, 'html.parser')
    for i in list(soup.find(id='PageContentDiv').find_all('table', {'class': 'tablo'})):
        if not i.caption.text.startswith('Niveau '):
            continue
        level = int(i.caption.text[len('Niveau '):])
        lines = i.find_all('tr')
        for line in lines:
            if 'titre' in line.get('class', []):
                continue
            
            monster = Monster()
            name, sous_type = line.find_all('td')
            if sous_type.text == 'Élémentaire':
                continue
            
            monster.name = parse_name(name)
            monster.align = parse_align(sous_type.text)
            monster.level = int(level)
            monster.link = parse_link(name)
            
            monsters[level].append(monster)
            monster.index = len(monsters[level])
    
    build_elem(monsters)
    build_mephite(monsters)
    
    return monsters


def read_in_line(line, monster, prop_string, prop_name):
    if prop_string in line and getattr(monster, prop_name) is None:
        for p in line.split(';'):
            p = p.strip()
            if p.startswith(prop_string):
                setattr(monster, prop_name, p[len(prop_string):].strip())
                break


def read_mephit_page(link):
    
    mephits = []
    page = get_page(link)
    soup = BeautifulSoup(page, 'html.parser')
    
    blocs_info = soup.find_all(class_='BD')
    for bloc_info in blocs_info:
        bloc_info_text_as_lines = list(format_distance(bloc_info.text).splitlines())
        bloc_info_text = [t_line for line in bloc_info_text_as_lines if (t_line := line.strip())]
        line = bloc_info_text.pop(0)
        if 'FP' in line:
            continue
        
        mephit = Mephit()
        name = line
        name, elem_type = name.split('(')
        mephit.name = name.strip()
        mephit.elem_type = elem_type[:-1].strip()
        
        while line:
            try:
                line = bloc_info_text.pop(0)
            except:
                break
            # print(line)
            if line.startswith('Vitesse :') and mephit.vd is None:
                mephit.vd = line[len('Vitesse :'):].strip()
                continue
            if line.startswith('Souffle :') and mephit.breath is None:
                mephit.breath = line[len('Souffle :'):].strip()
                continue
            if line.startswith('pouvoirs magiques :') or line.startswith('Pouvoirs magiques : ') and mephit.magic_powers is None:
                mephit.magic_powers = line[len('pouvoirs magiques :'):].strip()
                continue
            if line.startswith('Guérison accélérée :') and mephit.fast_healing is None:
                mephit.fast_healing = line[len('Guérison accélérée :'):].strip()
                continue
            if line.startswith('Faiblesse :') and mephit.weakness is None:
                mephit.weakness = line[len('Faiblesse :'):].strip()
                continue
            if line.startswith('Immunité :') and mephit.immunity is None:
                mephit.immunity = line[len('Immunité :'):].strip()
                continue
            if mephit.desc is None:
                mephit.desc = line
                continue
            if mephit.capa is None:
                mephit.capa = line
                continue
        
        mephits.append(mephit)
        mephit.index = len(mephits)
    
    for m in mephits:
        m.total = len(mephits)
        print(m.name)
    
    return mephits


def read_monster_page(monster):
    
    page = get_page(monster.link)
    soup = BeautifulSoup(page, 'html.parser')
    
    blocs_info = soup.find_all(class_='BD')
    for bloc_info in blocs_info:
        bloc_info_text_as_lines = list(format_distance(bloc_info.text).splitlines())
        bloc_info_text = [t_line for line in bloc_info_text_as_lines if (t_line := line.strip())]
        line = bloc_info_text.pop(0)
        
        # print(line)
        # print(bloc_info_text)
        if 'FP' not in line:
            continue
        name, fp = line.split('FP')
        if '(' in name:
            name, sub_name = name.split('(')
            monster.sub_name = sub_name[:-1]
        
        testing_name = monster.name.lower().strip()
        if monster.name == 'Fourmi géante, ouvrière':
            testing_name = 'fourmi géante'
        if monster.name == 'Fourmi géante, soldat':
            testing_name = 'fourmi géante'
        if monster.name == 'Fourmi géante, mâle':
            testing_name = 'fourmi géante'
        
        if name.lower().strip() != testing_name:
            continue
        monster.fp = fp.strip()
        
        while line.lower() != 'défense':
            line = bloc_info_text.pop(0)
            if (line.startswith('PX') or line.startswith('XP')) and monster.px is None:
                monster.px = int(line[len('PX'):].strip().replace('.', '').replace(' ', ''))
            if 'taille' in line and monster.size is None:
                monster.size = line.split()[-2].replace(',', '').strip()
                monster.raw_align = line.split()[-1].replace(',', '').strip()
            if line.startswith('Init') and monster.init is None:
                monster.init = line.split()[1].strip()
            if 'Sens' in line and monster.senses is None:
                for p in line.split(';'):
                    p = p.strip()
                    if p.startswith('Sens'):
                        monster.senses = p[len('Sens'):].strip()
            if 'Perception' in line and monster.perception is None:
                for p in line.split(';'):
                    p = p.strip()
                    if p.startswith('Perception'):
                        monster.perception = p[len('Perception'):].strip()
        
        while line.lower() != 'attaque':
            line = bloc_info_text.pop(0)
            # print(line)
            if line.lower() == 'attaque':
                continue
            if line.startswith('CA') and monster.ca is None:
                base_ca, touche_ca, surprised_ca = line.split(',', 2)
                monster.ca = base_ca.split()[1]
                monster.ca_touch = touche_ca.split()[1]
                monster.ca_surprised = surprised_ca.split()[3]
                continue
            if line.startswith('pv') and monster.pv is None:
                monster.pv = line[len('pv'):].strip()
                continue
            if line.startswith('Réf') and monster.saving_throw_ref is None:
                ref, vig, vol = line.split(',', 2)
                monster.saving_throw_ref = ref.split()[1]
                monster.saving_throw_vig = vig.split()[1]
                monster.saving_throw_vol = vol.split()[1]
                
                if ';' in vol:
                    monster.saving_throws = vol.split(';')[1]
                continue
            
            read_in_line(line, monster, 'Capacités défensives', 'capa_def')
            read_in_line(line, monster, 'Immunités', 'immunity')
            read_in_line(line, monster, 'RD', 'rd')
            read_in_line(line, monster, 'Résistances', 'resistance')
            read_in_line(line, monster, 'RM', 'rm')
            read_in_line(line, monster, 'Faiblesses', 'weakness')
            
            # print(line)
        
        in_magic_power = False
        in_spells = False
        while line.lower() != 'caractéristiques':
            line = bloc_info_text.pop(0)
            if line.lower() == 'caractéristiques':
                continue
            if line.startswith('VD') and monster.vd is None:
                monster.vd = line[len('VD'):].strip()
                continue
            if line.startswith('Corps à corps') and monster.cac is None:
                monster.cac = line[len('Corps à corps'):].strip()
                continue
            if line.startswith('Distance') and monster.distance is None:
                monster.distance = line[len('Distance'):].strip()
                continue
            if line.startswith('Espace') and monster.space is None:
                infos = line.split(';')
                monster.space = infos[0].strip()[len('Espace'):].strip()
                monster.reach = infos[1].strip()[len('Allonge'):].strip()
                continue
            if line.startswith('Attaques spéciales') and monster.attack_special is None:
                monster.attack_special = line[len('Attaques spéciales'):].strip()
                continue
            if line.startswith('Pouvoirs magiques') and monster.nls is None:
                # print('-------------------')
                # print(name)
                # print(line)
                monster.nls = line[len('Pouvoirs magiques (NLS '):-1]
                monster.magic_powers = []
                in_magic_power = True
                continue
            if line.startswith('Sorts préparés') and monster.spell_lvl is None:
                # print('-------------------')
                # print(name)
                # print(line)
                monster.spell_lvl = line[len('Sorts préparés (NLS '):-1]
                in_magic_power = False
                in_spells = True
                continue
            if line.startswith('Sorts connus') and monster.spell_lvl is None:
                # print('-------------------')
                # print(name)
                # print(line)
                monster.spell_lvl = line[len('Sorts connus (NLS '):-1]
                in_magic_power = False
                in_spells = True
                continue
            if in_spells:
                monster.spells.append(line)
                # print(line)
                continue
            if in_magic_power:
                monster.magic_powers.append(line)
                # print(line)
                continue
        
        while line.lower() != 'écologie':
            try:
                line = bloc_info_text.pop(0)
            except:
                break
            if line.lower() == 'écologie':
                continue
            
            if line.startswith('For') and monster.stats is None:
                monster.stats = line
                continue
            if line.startswith('BBA') and monster.bba is None:
                bba, bmo, dmd = line.split(',')
                monster.bba = bba[4:]
                monster.bmo = bmo[4:]
                monster.dmd = dmd[4:]
                continue
            if line.startswith('Dons') and monster.feats is None:
                monster.feats = line[len('Dons'):].strip()
                continue
            if line.startswith('Compétences') and monster.skill is None:
                monster.skill = line[len('Compétences'):].split(';')[0].strip()
                continue
            if line.startswith('Particularités') and monster.special is None:
                monster.special = line[len('Particularités'):].strip()
                continue
            if line.startswith('Particularité') and monster.special is None:
                monster.special = line[len('Particularité'):].strip()
                continue
            if line.startswith('Langues') and monster.languages is None:
                monster.languages = line[len('Langues'):].strip()
                continue
            
            # print(line)
        
        in_particularity = False
        while bloc_info_text:
            line = bloc_info_text.pop(0)
            
            if line.startswith('Environnement') and monster.environment is None:
                monster.environment = line[len('Environnement'):].strip()
                continue
            if line.startswith('Organisation sociale') and monster.social_organization is None:
                monster.social_organization = line[len('Organisation sociale'):].strip()
                continue
            if line.startswith('Trésor') and monster.treasure is None:
                monster.treasure = line[len('Trésor'):].strip()
                continue
            
            if line.lower() == 'pouvoirs spéciaux':
                in_particularity = True
                continue
            if line.lower() == 'particularités':
                in_particularity = True
                continue
            if line.lower() == 'capacités spéciales':
                in_particularity = True
                continue
            
            if in_particularity:
                monster.special_capa_info.append(line)
                continue
        
        if monster.name == 'Fourmi géante, ouvrière':
            monster.fp = '1'
            monster.cac = 'morsure, +3 (1d6+2), dard, +3 (1d4+2)'
            monster.special_capa_info = []
        if monster.name == 'Fourmi géante, mâle':
            monster.fp = '3'
            monster.vd = '15m(10c), escalade 6m(4c), vol 9m(6c) (moyenne)'
            monster.ca = '19'
            monster.ca_touch = '12'
            monster.ca_surprised = '17'
            monster.stats = 'For 18, Dex 14, Con 21, Int -, Sag 17, Cha 15'
            monster.pv = '22 (2d8+13)'
            monster.saving_throw_ref = '+2'
            monster.saving_throw_vig = '+6'
            monster.saving_throw_vol = '+1'
            monster.cac = 'morsure, +5 (1d6+4 et étreinte), dard, +5 (1d4+4 et poison)'
            monster.bba = '+3'
            monster.bmo = '+5 (+9 lutte)'
            monster.dmd = '16 (23 contre croc-en-jambe)'
            
            # print(monster)
        
        # print(monster)


def write_file(monsters, level):
    file_name = 'out/summon.csv' if level is None else 'out/summon-lvl-%s.csv' % level
    with open(file_name, 'w', newline='', encoding='utf-8') as csvfile:
        fieldnames = [k for k in Monster.__dict__.keys() if not k.startswith('_')]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        
        writer.writeheader()
        for monster in monsters:
            monster._format()  # noqa
            writer.writerow(monster.__dict__)


def write_file_mephit(mephits):
    file_name = 'out/mephit.csv'
    with open(file_name, 'w', newline='', encoding='utf-8') as csvfile:
        fieldnames = [k for k in Mephit.__dict__.keys() if not k.startswith('_')]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        
        writer.writeheader()
        for mephit in mephits:
            writer.writerow(mephit.__dict__)


def main():
    if not os.path.exists('out'):
        os.makedirs('out')
    if not os.path.exists('cache'):
        os.makedirs('cache')
    
    monsters = found_all_monsters_links()
    all_monsters = []
    # read_monster_page(monsters[1][0])
    
    for level, lists in monsters.items():
        for m in lists:
            m.total = len(lists)
            read_monster_page(m)
            all_monsters.append(m)
    
    for level, lists in monsters.items():
        write_file(lists, level)
    write_file(all_monsters, None)
    
    mephits = read_mephit_page('https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.M%C3%A9phite.ashx')
    write_file_mephit(mephits)


if __name__ == '__main__':
    main()
